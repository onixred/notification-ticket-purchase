package ru.maksimov.andrey.notification.ticket.purchase.controller;

import java.io.IOException;
import java.util.List;
import java.util.TimeZone;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ru.maksimov.andrey.commons.exception.SystemException;
import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.NotificationTicketPurchaseClient;
import ru.maksimov.andrey.notification.ticket.purchase.client.Response;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.StationDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;

/**
 * Тесты контрола
 * 
 * @author amaksimov
 */
public class CommonControllerTest {

	public static void main(final String args[]) throws Throwable {
		findAllByLogin();
		// findAllByStatusAndLogin();
		// findAllByName()
	}

	public static void findAllByLogin() throws IOException {
		String url = "http://localhost:8081" + "/rest" + NotificationTicketPurchaseClient.TIMETABLE_FIND_ALL_BY_LOGIN;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>("1000", headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Response<List<TimetableDto>>> response = restTemplate.exchange(url, HttpMethod.POST, entity,
				new ParameterizedTypeReference<Response<List<TimetableDto>>>() {
				});
		List<TimetableDto> timetablesDto = response.getBody().getResponse();
		for(TimetableDto timetableDto: timetablesDto) {
		String startTime = DateUtils.formatDate(timetableDto.getStartTime(), DateUtils.SHORT_TIME_FORMAT_RUSSIA, TimeZone.getTimeZone("GMT"));
		String endTime = DateUtils.formatDate(timetableDto.getEndTime(), DateUtils.SHORT_TIME_FORMAT_RUSSIA, TimeZone.getTimeZone("GMT"));
		StringBuilder timetableStr = new StringBuilder();
		timetableStr.append("%s");
		timetableStr.append(startTime);
		timetableStr.append("->");
		timetableStr.append(endTime);
		System.out.println(timetableStr.toString());
		}
		
		
	}

	public static void findAllByStatusAndLogin() throws IOException {
		String url = "http://localhost:8081" + "/rest"
				+ NotificationTicketPurchaseClient.TASK_FIND_ALL_BY_STATUS_AND_LOGIN + "1000";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		TaskStatus status = TaskStatus.NEW;
		HttpEntity<TaskStatus> entity = new HttpEntity<TaskStatus>(status, headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<TaskDto>> response = restTemplate.exchange(url, HttpMethod.POST, entity,
				new ParameterizedTypeReference<List<TaskDto>>() {
				});
		System.out.println(response.getBody());
	}

	public void findAllByName() throws SystemException {
		String url = "http://localhost:8081" + "/rest" + NotificationTicketPurchaseClient.STATION_FIND_ALL_BY_NAME;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>("АБАК", headers);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Response<List<StationDto>>> response = restTemplate.exchange(url, HttpMethod.POST, entity,
				new ParameterizedTypeReference<Response<List<StationDto>>>() {
				});
		System.out.println(response.getBody());
		;
	}

}
