package ru.maksimov.andrey.notification.ticket.purchase;

import ru.maksimov.andrey.notification.ticket.purchase.Starter;

/**
 * Тестовый стартер проекта
 * 
 * @author amaksimov
 */
public class TestStarter {

	public static void main(final String args[]) throws Throwable {
		Starter.main(args);
	}

}
