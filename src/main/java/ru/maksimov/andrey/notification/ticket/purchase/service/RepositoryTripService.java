package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Date;
import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "поездка"
 * 
 * @author amaksimov
 */
public interface RepositoryTripService {

	/**
	 * Добавить\обновить поездку в хранилище
	 * 
	 * @param trip
	 *            поездка
	 * @return структура поездка
	 */
	Trip addTrip(Trip trip);

	/**
	 * Найти поездку в хранилище по id
	 * 
	 * @param id
	 *            идентификатор
	 * @return поездка
	 */
	Trip findById(Long id);

	/**
	 * Найти список поездок по городам
	 * 
	 * @param stationStart
	 *            часть названия города
	 * @param stationEnd
	 *            часть названия города
	 * @param fromDate
	 *            часть названия города
	 * @return список городов
	 */
	List<Trip> findByStationAndDate(Station stationStart, Station stationEnd, Date fromDate);


	/**
	 * Найти список рейсов где дата отправления больше чем указанная дата
	 * 
	 * @param date
	 *            дата 
	 * @return список рейсов где дата отправления раньше чем указанная дата
	 */
	List<Trip> findAllByGreaterDate(Date date);

	/**
	 * Найти список идентификаторов рейсов где дата отправления больше чем указанная дата
	 * 
	 * @param date
	 *            дата отправки
	 * @return список идентификатопров рейсов где дата отправления раньше чем указанная дата
	 */
	List<Long> findAllIdByGreaterDate(Date date);

	/**
	 * Удалить рейс по идентификатору из хранилища.
	 * 
	 * @param id
	 *            идентификатор рейса
	 * @throws VerificationException 
	 */
	void delete(Trip trip);

	/**
	 * Удалить рейсы по списку идентификаторов из хранилища
	 * 
	 * @param id
	 *            список идентификаторов рейсов
	 */
	void delete(List<Long> ids);
}
