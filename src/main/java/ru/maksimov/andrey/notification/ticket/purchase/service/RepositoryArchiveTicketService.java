package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "арив билетов"
 * 
 * @author amaksimov
 */
public interface RepositoryArchiveTicketService {

	/**
	 * Добавить\обновить аривный билет в хранилище
	 * 
	 * @param archiveTicket
	 *            архивный билет
	 * @return билет
	 */
	ArchiveTicket addArchiveTicket(ArchiveTicket archiveTicket);

	/**
	 * Добавить\обновить список аривных билетов в хранилище
	 * 
	 * @param archiveTickets
	 *            список архивных билетов
	 * @return список билетов
	 */
	List<ArchiveTicket> addArchiveTickets(List<ArchiveTicket> archiveTickets);

	/**
	 * Найти все архивные билеты по списку идентификаторов 
	 * 
	 * @param archiveTicketIds
	 *            список идентификаторов
	 * @return список архивных билетов
	 */
	List<ArchiveTicket> findAllById(List<Long> archiveTicketIds);

	/**
	 * Найти архивный билет по идентификатору
	 * 
	 * @param archiveTicketId
	 *            идентификатор билета
	 * @return архивный билет
	 */
	ArchiveTicket findById(Long archiveTicketId);

}
