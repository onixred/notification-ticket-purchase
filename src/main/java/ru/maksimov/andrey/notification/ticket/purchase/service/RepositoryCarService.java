package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Date;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "вагон"
 * 
 * @author amaksimov
 */
public interface RepositoryCarService {

	/**
	 * Найти вагон в хранилище номеру и поезду
	 * 
	 * @param number
	 *            номер вагона
	 * @param stationStartDate
	 *            номер поезда
	 * @param stationStartDate
	 *            дате оправления со станции
	 * @return вагон
	 */
	Car findByNumberAndDate(String number, String numberTrain, Date stationStartDateTrain);

	/**
	 * Добавить\обновить вагон в хранилище
	 * 
	 * @param car
	 *            вагон
	 * @return вагон
	 */
	Car addCar(Car car);
	
}
