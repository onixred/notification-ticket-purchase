package ru.maksimov.andrey.notification.ticket.purchase.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;

/**
 * Интерфейс по работе c хранилищем пользователь
 * 
 * @author amaksimov
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	@Query("FROM User u where u.login = ?1")
	User findByLogin(String login);
}
