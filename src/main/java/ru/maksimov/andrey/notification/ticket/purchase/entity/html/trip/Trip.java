package ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Сущность маршрут РЖД
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trip {

	private String fromStation;
	private long fromStationCode;
	private String whereStation;
	private long whereStationCode;
	private Date startDate;
	private Train[] trains;

	@JsonProperty("from")
	public String getFromStation() {
		return fromStation;
	}

	public void setFromStation(String fromStation) {
		this.fromStation = fromStation;
	}

	@JsonProperty("fromCode")
	public long getFromStationCode() {
		return fromStationCode;
	}

	public void setFromStationCode(long fromStationCode) {
		this.fromStationCode = fromStationCode;
	}

	@JsonProperty("where")
	public String getWhereStation() {
		return whereStation;
	}

	public void setWhereStation(String whereStation) {
		this.whereStation = whereStation;
	}

	@JsonProperty("whereCode")
	public long getWhereStationCode() {
		return whereStationCode;
	}

	public void setWhereStationCode(long whereStationCode) {
		this.whereStationCode = whereStationCode;
	}

	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateUtils.SHORT_DATE_FORMAT_RUSSIA, timezone = "CET")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonProperty("list")
	public Train[] getTrains() {
		return trains;
	}

	public void setTrains(Train[] trains) {
		this.trains = trains;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
