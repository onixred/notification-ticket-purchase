package ru.maksimov.andrey.notification.ticket.purchase.repository;

import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Интерфейс по работе c хранилищем поездка
 * 
 * @author amaksimov
 */
@Repository
public interface TripRepository extends CrudRepository<Trip, Long> {

	@Query("FROM Trip t where t.stationStart = ?1 and t.stationEnd = ?2 and t.date = ?3")
	List<Trip> findByStation(Station stationStart, Station stationEnd, Date fromDate);


	@Query("from Trip t "
			+ "where t.date < ?1")
	List<Trip> findAllByGreaterDate(Date departureTrain);

	@Query("select t.id from Trip t "
			+ "where t.date < ?1")
	List<Long> findAllIdByGreaterDate(Date departureTrain);

	@Query("DELETE FROM Trip t WHERE t.id in ?1")
	void delete(List<Long> ids);
}
