package ru.maksimov.andrey.notification.ticket.purchase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;

/**
 * Интерфейс по работе c хранилищем "критерии задачи"
 * 
 * @author amaksimov
 */
@Repository
public interface CriterionRepository extends CrudRepository<Criterion, Long> {

}
