package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;
import ru.maksimov.andrey.notification.ticket.purchase.repository.CarRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryCarService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "вагон"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryCarServiceImpl implements RepositoryCarService {

	@Autowired
	private CarRepository сarRepository;

	@Override
	public Car findByNumberAndDate(String number, String numberTrain, Date stationStartDateTrain) {
		//TODO реализовать
		return null;
	}

	@Override
	public Car addCar(Car car) {
		return сarRepository.save(car);
	}

}
