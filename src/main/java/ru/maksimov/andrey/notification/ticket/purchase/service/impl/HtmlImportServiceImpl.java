package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonMappingException;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.config.Config;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.Rid;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.ResponseTrain;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Train;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.ResponseTrip;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Trip;
import ru.maksimov.andrey.notification.ticket.purchase.service.HtmlImportService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.HtmlUtil;

/**
 * Сервис по работе с импортом html
 * 
 * @author amaksimov
 */
@Service
public class HtmlImportServiceImpl implements HtmlImportService {

	private static final Logger LOG = LogManager.getLogger(HtmlImportServiceImpl.class);
	private static String PARM_NAME_PART_CITY_FIND = "stationNamePart";
	private static String PARM_LANG_CITY_FIND = "lang";

	private Config config = Config.getConfig();

	@Loggable(result = false)
	public Station[] findAllByName(String stationName) {
		// TODO нужны проверки
		Station[] stations = doFindStations(stationName);
		return stations;
	}

	@Loggable(result = false)
	public Trip[] findAllByStationAndDate(Station stationStart, Station stationEnd, Date fromDate) {
		// TODO нужны проверки
		Trip[] trips;
		ResponseTrip responseTrip = doFindtTrips(stationStart, stationEnd, fromDate);
		if (responseTrip != null && responseTrip.getTrips() != null) {
			trips = responseTrip.getTrips();
		} else {
			trips = new Trip[0];
		}
		return trips;
	}

	public Train findByStationAndDateAndTrainNumber(Station stationStart, Station stationEnd, Date fromDate, String trainNumber) {
		// TODO нужны проверки
		Train train;
		ResponseTrain responseTrain = doFindtTrain(stationStart, stationEnd, fromDate, trainNumber);
		if (responseTrain != null && responseTrain.getTrains() != null) {
			Train[] trains = responseTrain.getTrains();
			if(trains.length > 1) {
				LOG.error("Warning received more trains than expected. Trains: " + Arrays.toString(trains));
			}
			train = trains[0];
		} else {
			train = new Train();
		}
		return train;
	}

	@Loggable(result = false)
	private ResponseTrip doFindtTrips(Station stationStart, Station stationEnd, Date fromDate) {
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.SHORT_DATE_FORMAT_RUSSIA);

		HttpPost httpClient = new HttpPost(config.getUrlPostBase() + "?layer_id=5827");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("tfl", "1"));
		nvps.add(new BasicNameValuePair("st0", stationStart.getName()));
		nvps.add(new BasicNameValuePair("code0", Long.toString(stationStart.getCode())));
		nvps.add(new BasicNameValuePair("st1", stationEnd.getName()));
		nvps.add(new BasicNameValuePair("code1", Long.toString(stationEnd.getCode())));
		nvps.add(new BasicNameValuePair("dir", "0"));
		nvps.add(new BasicNameValuePair("dt0", sdf.format(fromDate)));
		nvps.add(new BasicNameValuePair("dt1", sdf.format(fromDate)));
		nvps.add(new BasicNameValuePair("checkSeats", "1"));
		HtmlUtil.setHeader(httpClient, config.getHeaders());
		ResponseTrip trips = new ResponseTrip();
		try {
			trips = doPostExecute(httpClient, nvps, ResponseTrip.class, 1);
		} catch (ClientProtocolException e) {
			LOG.error("Unable get 'Trip': stationNameStart = " + stationStart.getName() + " stationNameEnd = "
					+ stationEnd.getName() + " dateStart = " + fromDate, e);
		} catch (IOException e) {
			LOG.error("Unable get 'Trip': stationNameStart = " + stationStart.getName() + " stationNameEnd = "
					+ stationEnd.getName() + " dateStart = " + fromDate, e);
		}
		return trips;
	}

	@Loggable(result = false)
	private ResponseTrain doFindtTrain(Station stationStart, Station stationEnd, Date fromDate, String trainNumber) {
		ResponseTrain train = new ResponseTrain();
		HttpPost httpClient = new HttpPost(config.getUrlPostBase() + "?layer_id=5764");
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("dir", "0"));
		nvps.add(new BasicNameValuePair("code0", String.valueOf(stationStart.getCode())));
		nvps.add(new BasicNameValuePair("code1", String.valueOf(stationEnd.getCode())));
		nvps.add(new BasicNameValuePair("dt0", DateUtils.formatDate(fromDate, DateUtils.SHORT_DATE_FORMAT_RUSSIA)));
		nvps.add(new BasicNameValuePair("tnum0", trainNumber));
		HtmlUtil.setHeader(httpClient, config.getHeaders());
		try {
			train = doPostExecute(httpClient, nvps, ResponseTrain.class, 1);
		} catch (ClientProtocolException e) {
			LOG.error("Unable get 'Train': stationNameStart = " + stationStart.getName() + " stationNameEnd = "
					+ stationEnd.getName() + " dateStart = " + fromDate + " trainNumber = " + trainNumber, e);
		} catch (IOException e) {
			LOG.error("Unable get 'Train': stationNameStart = " + stationStart.getName() + " stationNameEnd = "
					+ stationEnd.getName() + " dateStart = " + fromDate + " trainNumber = " + trainNumber, e);
		}
		return train;
	}

	@Loggable(result = false)
	private Station[] doFindStations(String stationName) {
		HttpUriRequest httpClient;
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair(PARM_NAME_PART_CITY_FIND, stationName));
		nvps.add(new BasicNameValuePair(PARM_LANG_CITY_FIND, "ru"));
		String query = URLEncodedUtils.format(nvps, StandardCharsets.UTF_8);
		httpClient = new HttpGet(config.getUrlGetCityFind() + "?" + query);
		HtmlUtil.setHeader(httpClient, config.getHeaders());
		Station[] partStations = new Station[0];
		try {
			partStations = doGetExecute(httpClient, Station[].class);
		} catch (ClientProtocolException e) {
			LOG.error("Unable get 'Stations': stationName = " + stationName, e);
		} catch (IOException e) {
			LOG.error("Unable get 'Stations': stationName = " + stationName, e);
		}
		return partStations;
	}

	@Loggable(result = false)
	private <T> T doPostExecute(HttpPost httpClient, List<NameValuePair> nvps, Class<T> aClass, int depth)
			throws ClientProtocolException, IOException {
		if(depth > 15) {
			throw new IOException("Error too large request depth.");
		} 
		T res = null;
		httpClient.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
		NameValuePair pairRid = HtmlUtil.findPairByName(nvps, "rid");
		int connectTimeout = pairRid != null ? config.getClientConfigConnectTimeoutMax()
				: config.getClientConfigConnectTimeoutRegular();
		int connectionRequestTimeout = pairRid != null ? config.getClientConfigConnectionRequestTimeoutMax()
				: config.getClientConfigConnectionRequestTimeoutRegular();
		int socketTimeout = pairRid != null ? config.getClientConfigSocketTimeoutMax()
				: config.getClientConfigSocketTimeoutRegular();
		connectionRequestTimeout+=(depth << 9); //depth * 512
		RequestConfig config = HtmlUtil.getConfig(connectTimeout, connectionRequestTimeout, socketTimeout);
		CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		CloseableHttpResponse responseHttp = httpclient.execute(httpClient);
		String response = EntityUtils.toString(responseHttp.getEntity());

		try {
			Rid rid = null;
			if (response.indexOf("\"result\":\"FAIL\"") > -1) {
				LOG.warn("Unable response :" + response.toString());
				return null;
			} else if (response.indexOf("\"result\":\"OK\"") > -1) {
				LOG.debug("aClass: " + aClass + " response: " + response);
				res = HtmlUtil.convertJson2Object(response, aClass);
				return res;
			} else {
				LOG.debug("RID, response: " + response);
				rid = HtmlUtil.convertJson2Object(response, Rid.class);
				if (pairRid == null) {
					nvps.add(new BasicNameValuePair("rid", rid.getRid()));
				}
			}
			//Ждем пока ржд выпалнит наш заказ
			try {
				Thread.sleep(3000 + (depth << 7)); // depth * 128
			} catch (InterruptedException e) {
				LOG.warn("Unable sleep thread! response:" + response.toString(), e);
			}
			// найти и записат сесию
			HtmlUtil.setSessionId(responseHttp.getHeaders("Set-Cookie"), httpClient);
			return doPostExecute(httpClient, nvps, aClass, depth + 1);
		} catch (JsonMappingException jpe) {
			LOG.warn("Unable parse json : " + response + " error " + jpe.getMessage());
			try {
				res = aClass.newInstance();
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
			return res;
		}
	}

	@Loggable(result = false)
	private <T> T doGetExecute(HttpUriRequest httpClient, Class<T> aClass) throws ClientProtocolException, IOException {
		T res = null;
		int connectTimeout = config.getClientConfigConnectTimeoutRegular();
		int connectionRequestTimeout = config.getClientConfigConnectionRequestTimeoutRegular();
		int socketTimeout = config.getClientConfigSocketTimeoutRegular();
		RequestConfig config = HtmlUtil.getConfig(connectTimeout, connectionRequestTimeout, socketTimeout);
		CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		CloseableHttpResponse responseHttp = httpclient.execute(httpClient);
		String response = EntityUtils.toString(responseHttp.getEntity());
		LOG.debug("aClass: " + aClass + " response: " + response);
		res = HtmlUtil.convertJson2Object(response, aClass);
		return res;
	}
}
