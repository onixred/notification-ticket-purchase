package ru.maksimov.andrey.notification.ticket.purchase.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;

/**
 * Интерфейс по работе c хранилищем поезд
 * 
 * @author amaksimov
 */
@Repository
public interface TrainRepository extends CrudRepository<Train, Long> {

	@Query("FROM Train t where t.number = ?1 and t.stationStartDate = ?2")
	Train findByNumber(String number, Date stationStartDate);
}
