package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.PlaceDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;

/**
 * Сущность место
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "place")
public class Place {

	@Id
	@GeneratedValue(generator = "placeSequenceGenerator")
	@GenericGenerator(name = "placeSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "PLACE_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), 
			@Parameter(name = "increment_size", value = "1") })
	private Long id;
	@Enumerated(EnumType.STRING)
	private PlaceType type;
	private int freeNumber;
	private double tariff;
	private double tariffAlternative;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern = DateUtils.BASE_DATE_FORMAT, timezone="Europe/Moscow")
	private Date dateUpdate;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "car_id")
	private Car car;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlaceType getType() {
		return type;
	}

	public void setType(PlaceType type) {
		this.type = type;
	}

	public int getFreeNumber() {
		return freeNumber;
	}

	public void setFreeNumber(int freeNumber) {
		this.freeNumber = freeNumber;
	}

	public double getTariff() {
		return tariff;
	}

	public void setTariff(double tariff) {
		this.tariff = tariff;
	}

	public double getTariffAlternative() {
		return tariffAlternative;
	}

	public void setTariffAlternative(double tariffAlternative) {
		this.tariffAlternative = tariffAlternative;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public PlaceDto getDto() {
		PlaceDto placeDto = new PlaceDto();
		placeDto.setFreeNumber(freeNumber);
		placeDto.setTariff(tariff);
		placeDto.setTariffAlternative(tariffAlternative);
		placeDto.setType(type);
		placeDto.setDateUpdate(dateUpdate);
		return placeDto;
	}
}
