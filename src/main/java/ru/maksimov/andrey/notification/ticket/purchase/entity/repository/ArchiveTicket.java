package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;

/**
 * Сущность архив билетов для хранилищя
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "archive_ticket")
public class ArchiveTicket {

	@Id
	@GeneratedValue(generator = "archiveTicketSequenceGenerator")
	@GenericGenerator(name = "archiveTicketSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "ARCHIVE_TICKET_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1") })
	private Long id;

	@ManyToOne
	@JoinColumn(name = "station_start_id")
	private Station stationStart;
	@ManyToOne
	@JoinColumn(name = "station_end_id")
	private Station stationEnd;
	private Date dateTrip;
	@Enumerated(EnumType.STRING)
	private CarType carType;
	private String trainName;
	private String carName;
	private Date dateUpdate;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "archiveTicket", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ArchiveFreePlace> archiveFreePlaces = new HashSet<ArchiveFreePlace>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Station getStationStart() {
		return stationStart;
	}

	public void setStationStart(Station stationStart) {
		this.stationStart = stationStart;
	}

	public Station getStationEnd() {
		return stationEnd;
	}

	public void setStationEnd(Station stationEnd) {
		this.stationEnd = stationEnd;
	}

	public Date getDateTrip() {
		return dateTrip;
	}

	public void setDateTrip(Date dateTrip) {
		this.dateTrip = dateTrip;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public Set<ArchiveFreePlace> getArchiveFreePlaces() {
		return archiveFreePlaces;
	}

	public void setArchiveFreePlaces(Set<ArchiveFreePlace> archiveFreePlaces) {
		this.archiveFreePlaces = archiveFreePlaces;
	}

	public String getTrainName() {
		return trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

}
