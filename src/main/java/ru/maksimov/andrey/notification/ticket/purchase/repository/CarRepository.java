package ru.maksimov.andrey.notification.ticket.purchase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;

/**
 * Интерфейс по работе c хранилищем вагонов
 * 
 * @author amaksimov
 */
@Repository
public interface CarRepository extends CrudRepository<Car, Long>{

}
