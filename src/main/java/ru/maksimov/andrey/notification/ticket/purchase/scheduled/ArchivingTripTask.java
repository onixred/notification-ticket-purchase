package ru.maksimov.andrey.notification.ticket.purchase.scheduled;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.maksimov.andrey.notification.ticket.purchase.client.TicketTelegramBotClient;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketsDto;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveFreePlace;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.commons.exception.SystemException;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.ArchiveTicketService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TripService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2DtoUtil;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2RunnableUtil;

/**
 * Поток (задача) архивировать билеты
 * 
 * @author amaksimov
 */
@Component
@Scope("prototype")
public class ArchivingTripTask implements Runnable {

	private static final Logger LOG = LogManager.getLogger(ArchivingTripTask.class);

	@Autowired
	private TripService tripService;

	@Autowired
	private ArchiveTicketService archiveTicketService;

	@Autowired
	private TicketTelegramBotClient ticketTelegramBotClient;

	private Long tripId;
	private Long archiveTicketId;

	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	public void setArchiveTicketId(Long archiveTicketId) {
		this.archiveTicketId = archiveTicketId;
	}

	@Override
	public void run() {
		List<ArchiveTicket> archiveTickets = null;
		if (archiveTicketId != null) {
			archiveTickets = new ArrayList<ArchiveTicket>();
			try {
				ArchiveTicket archiveTicket = archiveTicketService.findById(archiveTicketId);
				archiveTickets.add(archiveTicket);
			} catch (VerificationException ve) {
				LOG.info("Skip archive ticket unable find archiveTicket by archiveTicketId : " + archiveTicketId
						+ " error " + ve.getMessage());
			}
		} else if (tripId != null) {
			Trip trip;
			try {
				trip = tripService.findById(tripId);
				archiveTickets = ConvertEntity2RunnableUtil.convertTrip2ArchiveTickets(trip);
			} catch (VerificationException ve) {
				LOG.info("Skip archive ticket unable find trip by tripId : " + tripId + " error " + ve.getMessage());
			}

		}

		if (tripId == null) {
			tripId = new Long(-1L);
		}
		synchronized (tripId) {
			try {
				if (archiveTickets == null || archiveTickets.isEmpty()) {
					LOG.info("Skip archive ticket is empty tripId: " + tripId);
					return;
				}
				// решаем баг в бд в архиве повторы сплюснусь список!
				LOG.info("Count archive ticket is: " + archiveTickets.size());
				List<ArchiveTicket> archiveTicketsDistinct = distinct(archiveTickets);
				LOG.info("Count distinct archive ticket is: " + archiveTicketsDistinct.size());
				if (!archiveTicketsDistinct.isEmpty()) {
					if (!tripId.equals(-1L)) {
						archiveTicketService.addArchiveTickets(archiveTicketsDistinct);
					}
					List<ArchiveTicketDto> listArchiveTickets = new ArrayList<ArchiveTicketDto>();
					for (ArchiveTicket archiveTicket : archiveTicketsDistinct) {
						ArchiveTicketDto archiveTicketDto = ConvertEntity2DtoUtil
								.convertArchiveTickets2ArchiveTicketsDto(archiveTicket);
						listArchiveTickets.add(archiveTicketDto);
					}
					try {
						ArchiveTicketsDto archiveTicketsDto = new ArchiveTicketsDto();
						archiveTicketsDto.setListArchiveTicketDto(listArchiveTickets);
						ticketTelegramBotClient.archiveTicketsSend(archiveTicketsDto);
					} catch (SystemException e) {
						LOG.error("Unable send archive tickets. tripId: " + tripId + " " + e.getMessage(), e);
					}
				}

				if (!tripId.equals(-1L)) {
					tripService.delete(tripId);
				}

			} catch (VerificationException ve) {
				LOG.error("Unable task load Trip. tripId: " + tripId);
			}
		}
	}

	private List<ArchiveTicket> distinct(List<ArchiveTicket> archiveTickets) {
		List<ArchiveTicket> archiveTicketsDistinct = new ArrayList<ArchiveTicket>();
		Map<String, ArchiveTicket> key2ArchiveTicket = new HashMap<String, ArchiveTicket>();
		for (ArchiveTicket archiveTicket : archiveTickets) {
			StringBuilder key = new StringBuilder();
			key.append(archiveTicket.getTrainName());
			key.append(archiveTicket.getCarName());
			key.append(archiveTicket.getStationStart().getCode());
			key.append(archiveTicket.getStationEnd().getCode());
			key.append(archiveTicket.getDateTrip().getTime());
			key.append(archiveTicket.getCarType().getName());
			ArchiveTicket archiveTicketSave = key2ArchiveTicket.get(key.toString());
			if (archiveTicketSave == null) {
				key2ArchiveTicket.put(key.toString(), archiveTicket);
			} else {
				archiveTicketSave.getArchiveFreePlaces();
				Set<ArchiveFreePlace> mergePlaces = merge(archiveTicket.getArchiveFreePlaces(),
						archiveTicketSave.getArchiveFreePlaces(), archiveTicketSave);
				archiveTicketSave.setArchiveFreePlaces(mergePlaces);
			}
		}
		for (ArchiveTicket archiveTicket : key2ArchiveTicket.values()) {
			archiveTicketsDistinct.add(archiveTicket);
		}
		return archiveTicketsDistinct;
	}

	private Set<ArchiveFreePlace> merge(Set<ArchiveFreePlace> archiveFreePlaces1,
			Set<ArchiveFreePlace> archiveFreePlaces2, ArchiveTicket archiveTicket) {
		Set<ArchiveFreePlace> archiveFreePlaces = new HashSet<ArchiveFreePlace>();
		Map<String, ArchiveFreePlace> key2archiveFreePlace = new HashMap<String, ArchiveFreePlace>();
		archiveFreePlaces1.addAll(archiveFreePlaces2);
		for (ArchiveFreePlace archiveFreePlace : archiveFreePlaces1) {
			StringBuilder key = new StringBuilder();
			key.append(archiveFreePlace.getPlaceType().getName());
			key.append(archiveFreePlace.getDateUpdate().getTime());
			ArchiveFreePlace archiveFreePlaceSave = key2archiveFreePlace.get(key.toString());
			if (archiveFreePlaceSave == null) {
				key2archiveFreePlace.put(key.toString(), archiveFreePlace);
			} else {
				String trainName = archiveFreePlaceSave.getArchiveTicket().getTrainName();
				String carName = archiveFreePlaceSave.getArchiveTicket().getCarName();
				String stationStar = archiveFreePlaceSave.getArchiveTicket().getStationStart().getName();
				String stationEnd = archiveFreePlaceSave.getArchiveTicket().getStationEnd().getName();
				String dateTrip = archiveFreePlaceSave.getArchiveTicket().getDateTrip().toString();
				String placeType = archiveFreePlaceSave.getPlaceType().getMessage();
				String freeNumber = Integer.toString(archiveFreePlaceSave.getFreeNumber());
				LOG.warn("Duplicate found, trainName: " + trainName + ", carName: " + carName + ", stationStar"
						+ stationStar + ", stationEnd: " + stationEnd + ", dateTrip: " + dateTrip + ", placeType: "
						+ placeType + ", freeNumber: " + freeNumber);
			}
		}
		for (ArchiveFreePlace archiveFreePlace : key2archiveFreePlace.values()) {
			archiveFreePlaces.add(archiveFreePlace);
			archiveFreePlace.setArchiveTicket(archiveTicket);
		}
		return archiveFreePlaces;
	}
}
