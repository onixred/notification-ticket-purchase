package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Date;
import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "Задача"
 * 
 * @author amaksimov
 */
public interface RepositoryTaskService {

	/**
	 * Найти список задач по статусу и дате
	 * 
	 * @param status
	 *            статус задачи
	 * @param updateIntervalMin
	 *            интервал обновления (в минутах), для сравнения с датой
	 *            обновления свободных мест
	 * @param openingPeriodTrip
	 *            периуд открытия билетоав (в сутках). дата рейса не должна быть
	 *            раньше чем date - openingPeriodTrip)
	 * @param date
	 *            не рааньше заданой даты
	 * @return список задач
	 */
	List<Task> findAllByStatusAndDate(TaskStatus status, int updateIntervalMin, int openingPeriodTrip, Date date);

	/**
	 * Найти список идентификаторов задач по статусу и дате
	 * 
	 * @param status
	 *            статус задачи
	 * @param updateIntervalMin
	 *            интервал обновления (в минутах), для сравнения с датой
	 *            обновления свободных мест
	 * @param openingPeriodTrip
	 *            периуд открытия билетоав (в сутках). дата рейса не должна быть
	 *            раньше чем date - openingPeriodTrip)
	 * @param date
	 *            не рааньше заданой даты
	 * @return список идентификаторов задач
	 */
	List<Long> findAllTaskIdByStatusAndDate(TaskStatus status, int updateIntervalMin, int openingPeriodTrip, Date date);
	
	
	/**
	 * Найти список задач по статусу и дате и где нету даты отправки
	 * 
	 * @param status
	 *            статус задачи
	 * @param openingPeriodTrip
	 *            периуд открытия билетоав (в сутках). дата рейса не должна быть
	 *            раньше чем date - openingPeriodTrip)
	 * @param date
	 *            не рааньше заданой даты
	 * @return список задач
	 */
	List<Task> findAllByStatusAndDate(TaskStatus status, int openingPeriodTrip, Date date);

	/**
	 * Найти список идентификаторов задач по статусу и дате и где нету даты отправки
	 * 
	 * @param status
	 *            статус задачи
	 * @param openingPeriodTrip
	 *            периуд открытия билетоав (в сутках). дата рейса не должна быть
	 *            раньше чем date - openingPeriodTrip)
	 * @param date
	 *            не рааньше заданой даты
	 * @return список задач
	 */
	List<Long> findAllTaskIdByStatusAndDate(TaskStatus status, int openingPeriodTrip, Date date);

	/**
	 * Найти задачу в хранилище по id
	 * 
	 * @param id
	 *            идентификатор
	 * @return задача
	 */
	Task findById(Long id);

	/**
	 * Добавить\обновить задачу
	 * 
	 * @param task
	 *            задача
	 * @return задача
	 */
	Task addTask(Task task);

	/**
	 * Найти все задачи по состоянию и логину 
	 * 
	 * @param status
	 *            статус задачи
	 * @param login
	 *            логин пользователя
	 * @return список задач
	 */
	List<Task> findAllByStatusAndLogin(TaskStatus status, String login);

	/**
	 * Получить количество задач по состоянию и логину 
	 * 
	 * @param status
	 *            статус задачи
	 * @param login
	 *            логин пользователя
	 * @return число задач
	 */
	Integer getCountTaskByStatusAndLogin(TaskStatus status, String login);

	/**
	 * Получить количество задач по состоянию
	 * 
	 * @param status
	 *            статус задачи
	 * @return число задач
	 */
	Integer getCountTaskByStatus(TaskStatus status);

	/**
	 * Найти полную информацию о задаче, критериях, рейсе и тд (от задачи до свободных места) в хранилище по id
	 * 
	 * @param id
	 *            идентификатор
	 * @return полная информация о  задача
	 */
	Task findFullTaskById(Long taskId);
}
