package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.ArchiveTicketService;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryArchiveTicketService;

/**
 * Сервис по работе с сущностью "арив билетов"
 * 
 * @author amaksimov
 */
@Service
public class ArchiveTicketServiceImpl implements ArchiveTicketService {

	@Autowired
	private RepositoryArchiveTicketService repositoryArchiveTicketService;

	@Override
	public ArchiveTicket addArchiveTicket(ArchiveTicket archiveTicket) throws VerificationException {
		if (archiveTicket == null) {
			throw new VerificationException("Unable add ArchiveTicket. ArchiveTicket is null");
		}
		ArchiveTicket repositoryArchiveTicket = repositoryArchiveTicketService.addArchiveTicket(archiveTicket);
		return repositoryArchiveTicket;
	}

	@Loggable
	@Override
	public List<ArchiveTicket> addArchiveTickets(List<ArchiveTicket> archiveTickets) throws VerificationException {
		if (archiveTickets == null) {
			throw new VerificationException("Unable add ArchiveTicket's. ArchiveTicket's is null");
		}
		List<ArchiveTicket> repositoryArchiveTickets = repositoryArchiveTicketService.addArchiveTickets(archiveTickets);
		return repositoryArchiveTickets;
	}

	@Override
	public List<ArchiveTicket> findAllById(List<Long> archiveTicketIds) throws VerificationException {
		if (archiveTicketIds == null || archiveTicketIds.isEmpty()) {
			throw new VerificationException("Unable find all ArchiveTicket's. ArchiveTicket id's is null");
		}
		List<ArchiveTicket> repositoryArchiveTickets = repositoryArchiveTicketService.findAllById(archiveTicketIds);
		return repositoryArchiveTickets;
	}
	
	@Override
	public ArchiveTicket findById(Long archiveTicketId) throws VerificationException {
		if (archiveTicketId == null) {
			throw new VerificationException("Unable find ArchiveTicket. archiveTicketId is null");
		}
		return repositoryArchiveTicketService.findById(archiveTicketId);
	}
}
