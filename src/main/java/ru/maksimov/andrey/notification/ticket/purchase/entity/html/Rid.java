package ru.maksimov.andrey.notification.ticket.purchase.entity.html;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Сущность rid ключ РЖД
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rid {

	private String result;
	private String rid;
	private Date timestamp;

	@JsonProperty("result")
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@JsonProperty("RID")
	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	@JsonProperty("timestamp") // 13.12.2016 16:20:42.274
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateUtils.FULL_DATE_FORMAT, timezone = "CET")
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
