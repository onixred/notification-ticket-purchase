package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Set;

/**
 * Интерфейс сервиса по работе с обработчиком задач
 * 
 * @author amaksimov
 */
public interface TaskExecutorService {

	/**
	 * Добавить список задач в обработку.
	 * 
	 * @param tasks
	 *            список задач
	 * @return список городов
	 */
	void addTasks(Set<? extends Runnable> tasks);
}
