package ru.maksimov.andrey.notification.ticket.purchase.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Place;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;

/**
 * Вспомогаткельный класс для конвертации сущностей и html
 * 
 * @author amaksimov
 */
public class ConvertEntity2HtmlUtil {

	private static final Logger LOG = LogManager.getLogger(ConvertEntity2HtmlUtil.class);

	/**
	 * Конвертация станции из формат хранилища в формата сайта
	 * 
	 * @param station
	 *            станция в формате хранилища
	 * @return станция
	 */
	public static ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station convertStationRepository2Html(
			Station station) {
		ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station htmlStation = new ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station();
		htmlStation.setName(station.getName());
		htmlStation.setCode(station.getCode());
		return htmlStation;
	}

	/**
	 * Конвертация станции из формата сайта в формат хранилища
	 * 
	 * @param station
	 *            станция в формате сайта
	 * @return станция
	 */
	public static Station convertStationHtml2Repository(
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station station) {
		Station repositoryStation = new Station();
		repositoryStation.setName(station.getName());
		repositoryStation.setCode(station.getCode());
		return repositoryStation;
	}

	/**
	 * Конвертация рейса из формата сайта в формат хранилища
	 * 
	 * @param trip
	 *            рейс в формате сайта
	 * @param id
	 *            идентификатор рейса если есть
	 * @return рейс
	 */
	public static Trip convertTripHtml2Repository(
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Trip trip, Long id) {
		Trip repositoryTrip = new Trip();
		if (id != null) {
			repositoryTrip.setId(id);
		}
		repositoryTrip.setDate(trip.getStartDate());
		Station stationEnd = createRepositoryStation(trip.getWhereStation(), trip.getWhereStationCode());
		repositoryTrip.setStationEnd(stationEnd);
		Station stationStart = createRepositoryStation(trip.getFromStation(), trip.getFromStationCode());
		repositoryTrip.setStationStart(stationStart);
		String strTrip = HtmlUtil.convertObject2Json(trip);
		repositoryTrip.setDescription(strTrip);

		Set<Train> trains = new HashSet<Train>();
		for (ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Train train : trip.getTrains()) {
			Train repositoryTrain = convertTrainHtml2Repository(train, repositoryTrip);
			trains.add(repositoryTrain);
		}
		repositoryTrip.setTrains(trains);

		return repositoryTrip;
	}

	/**
	 * Конвертация поезда из формата сайта в формат хранилища
	 * 
	 * @param train
	 *            поезд в формате сайта
	 * @param trip
	 *            рейс в формате хранилища
	 * @return поезд
	 */
	public static Train convertTrainHtml2Repository(
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Train train, Trip trip) {
		Train repositoryTrain = new Train();
		repositoryTrain.setNumberAlternative(train.getNumberAlternative());
		repositoryTrain.setNumber(train.getNumber());
		// TODO сделать конвертацию типа нормальную в enum
		repositoryTrain.setType(Long.toString(train.getType()));
		repositoryTrain.setStationStartDate(setDate(train.getStartDate(), train.getStartTime()));
		repositoryTrain.setStationEndDate(setDate(train.getEndDate(), train.getEndTime()));

		repositoryTrain.setTrip(trip);
		return repositoryTrain;
	}

	/**
	 * Конвертация вагона из формата сайта в формат хранилища
	 * 
	 * @param car
	 *            вагон в формате сайта
	 * @param train
	 *            поезд в формате хранилища
	 * @return вагон
	 */
	public static Car convertCarHtml2Repository(
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Car car, Train train) {
		Car repositoryCar = findAndCreateCar(car, train.getCars());
		repositoryCar.setName(car.getCountNumber());
		CarType type = createCarType(car.getTypeLoc());
		repositoryCar.setType(type);
		repositoryCar.setTrain(train);
		repositoryCar.setPlaceNumbers(car.getPlaces());
		repositoryCar.setClassType(car.getClassType());
		Set<Place> places = new HashSet<Place>();
		Date date = new Date();
		for (ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Seat seet : car.getSeats()) {
			double tariff = car.getTariff() != null ? car.getTariff() : 0D;
			double tariffAlternative = car.getTariffAlternative() != null ? car.getTariffAlternative() : 0D;
			Place repositorySeat = convertSeatHtml2Repository(seet, repositoryCar, tariff, tariffAlternative, date);
			if (repositorySeat != null) {
				// нужно обновить данные
				places.add(repositorySeat);
			}
		}
		if (repositoryCar.getPlaces() == null) {
			repositoryCar.setPlaces(places);
		} else {
			repositoryCar.getPlaces().addAll(places);
		}

		return repositoryCar;
	}

	/**
	 * Перевод строки в CarType
	 * 
	 * @param type
	 *            строка с типом
	 * @return тип вагона
	 */
	public static CarType createCarType(String type) {
		CarType carType = null;
		for (CarType sType : CarType.values()) {
			if (type.toUpperCase().equals(sType.getName()) || type.toUpperCase().equals(sType.name())) {
				carType = sType;
			}
		}
		if (carType == null) {
			carType = CarType.OTHER;
			LOG.warn("Unable determine type " + type);
		}
		return carType;
	}

	/**
	 * Перевод строки в SeatType
	 * 
	 * @param type
	 *            строка с типом
	 * @return тип места
	 */
	public static PlaceType createSeatType(String type) {
		PlaceType seatType = null;
		for (PlaceType sType : PlaceType.values()) {
			if (type.toLowerCase().equals(sType.getName())) {
				seatType = sType;
			}
		}
		if (seatType == null) {
			seatType = PlaceType.OTHER;
			LOG.warn("Unable determine type " + type);
		}
		return seatType;
	}

	/**
	 * Конвертация мест из формата сайта в формат хранилища
	 * 
	 * @param seat
	 *            место в формате сайта
	 * @param car
	 *            вагон в формате хранилища
	 * @param tariff
	 *            тариф за место
	 * @param tariffAlternative
	 *            алтернативный тариф за место
	 * @param date
	 *            дата выгрузки данных
	 * @return станция, мжет быть null если данные не изменились
	 */
	public static Place convertSeatHtml2Repository(
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Seat seat, Car car, double tariff,
			double tariffAlternative, Date date) {
		// TODO Сделать учет цены если купе то верх это алтернатива если
		// плацкарт то верх край это альтентатива.
		PlaceType type = createSeatType(seat.getType());
		boolean isAdd = true;
		for (Place place : car.getPlaces()) {
			if (place.getType().equals(type) && place.getFreeNumber() == seat.getFree()) {
				isAdd = false;
				break;
			}
		}
		Place repositorySeat = null;
		if (isAdd) {
			repositorySeat = new Place();
			repositorySeat.setType(type);
			repositorySeat.setFreeNumber(seat.getFree());
			repositorySeat.setTariff(tariff);
			repositorySeat.setTariffAlternative(tariffAlternative);
			repositorySeat.setDateUpdate(date);
			repositorySeat.setCar(car);
		}
		return repositorySeat;
	}

	/**
	 * Создать станцию в формате хранилища
	 * 
	 * @param name
	 *            название станции
	 * @param code
	 *            код станции
	 * @return станция
	 */
	private static Station createRepositoryStation(String name, long code) {
		Station station = new Station();
		station.setCode(code);
		station.setName(name);
		return station;
	}

	/**
	 * Создать станцию в формате хранилища
	 * 
	 * @param id
	 *            идентификатор станции
	 * @return станция
	 */
	public static Station createRepositoryStation(long id) {
		Station station = new Station();
		station.setId(id);
		return station;
	}

	/**
	 * Найти вагон в наборе и обновить поля, если не нашли создать новый
	 * 
	 * @param htmlCar
	 *            вагон в формате сайта
	 * @param cars
	 *            набор вагонов в формате бд
	 * @return поезд
	 */
	private static Car findAndCreateCar(ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Car htmlCar,
			Set<Car> cars) {
		Car repositoryCar = null;
		for (Car car : cars) {
			if (car.getName().equals(htmlCar.getCountNumber())) {
				repositoryCar = car;
				break;
			}
		}
		if (repositoryCar == null) {
			repositoryCar = new Car();
		}
		return repositoryCar;
	}

	/**
	 * У дате обновляет время
	 * 
	 * @param date
	 *            дата
	 * @param tame
	 *            время в формате HH:mm
	 * @return обновленная дата
	 */
	private static Date setDate(Date date, String tame) {
		Calendar calendar = Calendar.getInstance();
		Calendar calendarTime = Calendar.getInstance();
		calendar.setTime(date);
		SimpleDateFormat formatter = new SimpleDateFormat(DateUtils.SHORT_TIME_FORMAT_RUSSIA);
		try {
			Date time = formatter.parse(tame);
			calendarTime.setTime(time);
		} catch (ParseException e) {
			LOG.warn("Unable parse tame. tame " + tame + " Message: " + e.getMessage());
		}
		calendar.set(Calendar.HOUR_OF_DAY, calendarTime.get(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, calendarTime.get(Calendar.MINUTE));
		return calendar.getTime();
	}
}
