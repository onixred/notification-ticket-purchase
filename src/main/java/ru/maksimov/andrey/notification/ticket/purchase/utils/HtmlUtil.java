package ru.maksimov.andrey.notification.ticket.purchase.utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Вспомогаткельный класс для html
 * 
 * @author amaksimov
 */
public class HtmlUtil {

	/**
	 * Конвертация Json строки в объект
	 * 
	 * @param str
	 *            строка
	 * @param aClass
	 *            класс объекта
	 * @return объект
	 */
	public static <T> T convertJson2Object(String str, Class<T> aClass) throws JsonMappingException {
		T user = null;
		ObjectMapper mapper = new ObjectMapper();
		// JSON from String to Object
		try {
			user = mapper.readValue(str, aClass);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			throw e;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * Конвертация объекта в Json строку
	 * 
	 * @param o
	 *            объект
	 * @return Json строка
	 */
	public static <T> String convertObject2Json(T o) {
		ObjectMapper mapper = new ObjectMapper();
		// Object to JSON in String
		String jsonInString = "";
		try {
			jsonInString = mapper.writeValueAsString(o);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}

	/**
	 * Найти идентификатора сесии в массиве "заголовков" и задать http-клиенту
	 * 
	 * @param headers
	 *            массив "заголовков"
	 * @param httpClientPost
	 *            http-клиент
	 */
	public static void setSessionId(Header[] headers, HttpPost httpClientPost) {
		String sessionId = "";
		for (Header header : headers) {
			if (header.getValue().indexOf("JSESSIONID") > -1) {
				sessionId = header.getValue();
				httpClientPost.addHeader("Cookie", sessionId);
				return;
			}
		}
	}

	/**
	 * Найти пару по имени
	 * 
	 * @param nvps
	 *            список пар
	 * @param name
	 *            имя пары
	 * @return пара, может быть null если пара не найдена
	 */
	public static NameValuePair findPairByName(List<NameValuePair> nvps, String name) {
		NameValuePair nameValuePair = null;
		for (NameValuePair pair : nvps) {
			if (pair.getName().equals(name)) {
				nameValuePair = pair;
				break;
			}
		}
		return nameValuePair;
	}

	/**
	 * Получить конфигурацию запроса
	 * 
	 * @param timeout
	 *            таймаут в мсек
	 * @return конфигурация запроса
	 */
	public static RequestConfig getConfig(int connectTimeout, int connectionRequestTimeout, int socketTimeout) {
		return RequestConfig.custom().setConnectTimeout(connectTimeout).setConnectionRequestTimeout(connectionRequestTimeout)
				.setSocketTimeout(socketTimeout).build();
	}

	/**
	 * Задать заголовок http-клиенту
	 * 
	 * @param httpClient
	 *            http-клиент
	 */
	public static void setHeader(HttpUriRequest httpClient, Map<String, List<String>> headers) {
		for (Entry<String, List<String>> header : headers.entrySet()) {
			List<String> values = header.getValue();
			int index = random(0, values.size());
			httpClient.addHeader(header.getKey(), values.get(index));
		}
		httpClient.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		// httpClient.addHeader("Accept-Encoding", "gzip, deflate, sdch, br");
		// httpClient.addHeader("Accept-Language",
		// "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
		httpClient.addHeader("Host", "m.rzd.ru");
		httpClient.addHeader("Referer", "http://www.m.rzd.ru/");
		httpClient.addHeader("Upgrade-Insecure-Requests", "1");
		httpClient.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		// httpClient.addHeader("User-Agent",
		// "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like
		// Gecko) Chrome/54.0.2840.99 Safari/537.36");
	}

	/**
	 * Получить случайное число в интервале
	 * 
	 * @param min
	 *            минимальная граница
	 * @param max
	 *            максимальная граница
	 * @return случайное число
	 */
	private static int random(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max);
	}

}
