package ru.maksimov.andrey.notification.ticket.purchase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;

/**
 * Интерфейс по работе c хранилищем расписания
 * 
 * @author amaksimov
 */
public interface TimetableRepository extends CrudRepository<Timetable, Long>{


	@Query("select t from User u "
			+ "inner join u.timetable as t "
			+ "WHERE u.login = ?1) ")
	List<Timetable> findAllTimetableByLogin(String login);

	@Query("select t from User u "
			+ "inner join u.timetable as t "
			+ "WHERE u.id = ?1) ")
	List<Timetable> findAllTimetableUserId(Long id);

	@Query("select COUNT(t) from User u "
			+ "inner join u.timetable as t "
			+ "where u.login = ?1) ")
	Integer getCountTimetableByLogin(String login);
}
