package ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;

/**
 * Сущность задача
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "task")
public class Task {

	@Id
	@GeneratedValue(generator = "taskSequenceGenerator")
	@GenericGenerator(name = "taskSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "TASK_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1") })
	private Long id;

	@Enumerated(EnumType.STRING)
	private TaskStatus status;
	private Date dateUpdate;
	private Date dateSended;
	@ManyToOne
	@JoinColumn(name = "criteria_id")
	private Criterion criterion;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TaskStatus getStatus() {
		return status;
	}

	public void setStatus(TaskStatus status) {
		this.status = status;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	/**
	 *Получить дату отправления
	 */
	public Date getDateSended() {
		return dateSended;
	}

	public void setDateSended(Date dateSended) {
		this.dateSended = dateSended;
	}

	public Criterion getCriterion() {
		return criterion;
	}

	public void setCriterion(Criterion criterion) {
		this.criterion = criterion;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Сравнение без учета id и даты
	 * 
	 * @param task
	 *            задача
	 * @param isOverlap
	 *            признак если true допускать расхожения свойств, жесткое сравнение
	 * @return если объекты совпадают
	 */
	public boolean compare(Task task, boolean isOverlap) {
		if (task == this) {
			return true;
		}

		if (task == null) {
			return false;
		}

		if (!task.user.equals(user)) {
			return false;
		}

		if (!task.criterion.compare(criterion, isOverlap)) {
			return false;
		}
		return true;
	}
}
