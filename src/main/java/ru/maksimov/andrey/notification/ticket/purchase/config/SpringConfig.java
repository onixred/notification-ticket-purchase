package ru.maksimov.andrey.notification.ticket.purchase.config;

import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import ru.maksimov.andrey.notification.ticket.purchase.client.TicketTelegramBotClient;

/**
 * Конфигурация spting "
 * 
 * @author amaksimov
 */
@Configuration
@EnableJpaRepositories(basePackages = "ru.maksimov.andrey.notification.ticket.purchase.repository")
@EnableAutoConfiguration
@EntityScan(basePackages = { "ru.maksimov.andrey.notification.ticket.purchase.entity.repository" })
public class SpringConfig {

	private Config config = Config.getConfig();

	@Bean
	public TicketTelegramBotClient ticketTelegramBotClient() {
		return new TicketTelegramBotClient(config.getTicketTelegramBotUrlHost());
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate(clientHttpRequestFactory());
	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());
		factory.setReadTimeout(60000);
		factory.setConnectTimeout(2000);
		return factory;
	}
}
