package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.config.Config;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskExecutorService;

/**
 * Сервис по работе с обработчиком задач
 * 
 * @author amaksimov
 */
@Service
public class TaskExecutorServiceImpl implements TaskExecutorService {

	private Config config = Config.getConfig();

	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(config.getCorePoolSize());
		executor.setMaxPoolSize(config.getMaxPoolSize());
		executor.setQueueCapacity(config.getQueueCapacity());
		return executor;
	}

	@Autowired
	private TaskExecutor taskExecutor;

	@Override
	public void addTasks(Set<? extends Runnable> tasks) {
		for(Runnable task: tasks) {
			taskExecutor.execute(task);
		}
	}

}
