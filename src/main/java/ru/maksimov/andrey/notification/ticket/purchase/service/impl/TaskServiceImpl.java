package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TripDto;
import ru.maksimov.andrey.notification.ticket.purchase.config.Config;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.scheduled.NotificationTask;
import ru.maksimov.andrey.notification.ticket.purchase.service.CriterionService;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryTaskService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskExecutorService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TripService;
import ru.maksimov.andrey.notification.ticket.purchase.service.UserService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2DtoUtil;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertMessageUtil;

/**
 * Сервис по работе с сущностью "задача"
 * 
 * @author amaksimov
 */
@Service
public class TaskServiceImpl implements TaskService {

	private Config config = Config.getConfig();

	@Autowired
	private UserService userService;

	@Autowired
	private TripService tripService;

	@Autowired
	private CriterionService criterionService;

	@Autowired
	private RepositoryTaskService repositoryTaskService;

	@Autowired
	private TaskExecutorService taskExecutorService;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public TaskDto addTask(TaskDto taskDto) throws VerificationException {
		if (!сheckTaskDto(taskDto)) {
			throw new VerificationException("Unable add Task. TaskDto is incorrect.");
		}
		// поиск пользователя
		User saveUser = userService.doFindByLogin(taskDto.getLogin());
		Trip trip = ConvertEntity2DtoUtil.convertTaskDto2Trip(taskDto, saveUser);
		return doAddTask(trip);
	}

	@Loggable
	@Override
	public void findAllNewAndExecutionTasks() {
		int updateIntervalMin = config.getUpdateIntervalMin();
		int openingPeriodTrip = config.getOpeningPeriodTrip();
		List<Long> taskIds = repositoryTaskService.findAllTaskIdByStatusAndDate(TaskStatus.NEW, updateIntervalMin,
				openingPeriodTrip, new Date());
		List<Long> noSendTaskIds = repositoryTaskService.findAllTaskIdByStatusAndDate(TaskStatus.NEW, openingPeriodTrip,
				new Date());
		taskIds.addAll(noSendTaskIds);
		Set<Runnable> notificationTasks = new HashSet<Runnable>();
		for (Long taskId : taskIds) {
			NotificationTask notificationTask = applicationContext.getBean(NotificationTask.class);
			notificationTask.setTaskId(taskId);
			notificationTasks.add(notificationTask);
		}
		taskExecutorService.addTasks(notificationTasks);
	}

	@Loggable
	@Override
	public List<TaskDto> findAllByStatusAndLogin(TaskStatus status, String login) throws VerificationException {
		if (StringUtils.isBlank(login)) {
			throw new VerificationException("Unable find tasks by status and login. login is blank.");
		}
		List<TaskDto> tasksDto = new ArrayList<TaskDto>();
		List<Task> tasks = repositoryTaskService.findAllByStatusAndLogin(status, login);
		for (Task task : tasks) {
			TaskDto taskDto = ConvertEntity2DtoUtil.convertTask2TaskDto(task);
			tasksDto.add(taskDto);
		}
		return tasksDto;
	}

	@Loggable
	@Override
	public Integer getCountTaskByStatusAndLogin(TaskStatus status, String login) throws VerificationException {
		if (StringUtils.isBlank(login) || status == null) {
			throw new VerificationException("Unable find tasks by count status and login. login is blank.");
		}
		return repositoryTaskService.getCountTaskByStatusAndLogin(status, login);
	}

	@Loggable
	@Override
	public Integer getCountTaskByStatus(TaskStatus status) throws VerificationException {
		if (status == null) {
			throw new VerificationException("Unable find tasks by count status and login. login is blank.");
		}
		return repositoryTaskService.getCountTaskByStatus(status);
	}

	@Override
	public TaskDto findById(Long taskId) throws VerificationException {
		if (taskId == null) {
			throw new VerificationException("Unable find task. taskId is null.");
		}
		Task task = repositoryTaskService.findById(taskId);
		if (task == null) {
			throw new VerificationException("Unable find task.");
		}
		TaskDto taskDto = ConvertEntity2DtoUtil.convertTask2TaskDto(task);
		return taskDto;
	}

	@Override
	public Task doFindById(Long taskId) {
		return repositoryTaskService.findById(taskId);
	}

	@Override
	public Task addTask(Task task) {
		return repositoryTaskService.addTask(task);
	}

	/**
	 * Найти рейс и добавить задачу, если нет рейса то создать новый рейс
	 * 
	 * @param trip
	 *            рейс с информацией о задаче
	 * @return рейс сохраненный в хранилище
	 * @throws VerificationException
	 */
	private TaskDto doAddTask(Trip trip) throws VerificationException {
		List<Trip> trips = tripService.findAllByStationAndDate(trip.getStationStart().getId(),
				trip.getStationEnd().getId(), trip.getDate(), true);
		Criterion updateСriterion;
		Task task;
		if (trips.isEmpty()) {
			updateСriterion = trip.getCriteria().iterator().next();
			task = updateСriterion.getTasks().iterator().next();
		} else if (trips.size() > 1) {
			throw new VerificationException("Unable find Trip. The number of trip:" + trips.size() + " stationStartId "
					+ trip.getStationStart().getId() + " stationEndId " + trip.getStationEnd().getId() + " date "
					+ trip.getDate());
		} else {
			updateСriterion = trip.getCriteria().iterator().next();
			Trip saveTrip = trips.iterator().next();
			// проверка возможно уже есть такая задача
			Criterion criterion = findСriterion(saveTrip.getCriteria(), updateСriterion);
			if (criterion != null) {
				throw new VerificationException("Unable add task. Task exists. " + "Count place: "
						+ criterion.getCountPlace() + ", type car: " + criterion.getCarType().getName()
						+ ", type place: " + criterion.getPlaceType() + ", start station: "
						+ criterion.getTrip().getStationStart().getName() + ", end station: "
						+ criterion.getTrip().getStationEnd().getName() + ", date: " + criterion.getTrip().getDate());
			}
			updateСriterion.setTrip(saveTrip);
			task = updateСriterion.getTasks().iterator().next();
		}

		Criterion saveCriterion = criterionService.addCriterion(updateСriterion);
		Task saveTask = findTask(saveCriterion.getTasks(), task);
		TaskDto taskDto = ConvertEntity2DtoUtil.convertTask2TaskDto(saveTask);
		return taskDto;
	}

	/**
	 * Найти критерии задачи среди списка
	 * 
	 * @param criteria
	 *            список критериев
	 * @param updateСriterion
	 *            критерии задачи
	 * @return найденый критерий, если не найден то null
	 */
	private Criterion findСriterion(Set<Criterion> criteria, Criterion updateСriterion) {
		Criterion resCriterion = null;
		if (criteria != null || updateСriterion != null) {
			for (Criterion criterion : criteria) {
				if (updateСriterion.compare(criterion, false)) {
					resCriterion = criterion;
					break;
				}
			}
		}
		return resCriterion;
	}

	/**
	 * Найти задачe среди списка
	 * 
	 * @param tasks
	 *            список задач
	 * @param task
	 *            задача
	 * @return найденая задача, если не найдена то null
	 */
	private Task findTask(Set<Task> tasks, Task task) {
		Task resTask = null;
		if (tasks != null || task != null) {
			for (Task saveTask : tasks) {
				if (task.compare(saveTask, true)) {
					resTask = saveTask;
					break;
				}
			}
		}
		return resTask;
	}

	/**
	 * Проверка корректности формы "Задача".
	 * 
	 * @param taskDto
	 *            форма задача
	 * @return признак равен trur данные корректный иначе false
	 */
	private boolean сheckTaskDto(TaskDto taskDto) {
		boolean isValidData = true;
		if (taskDto == null) {
			isValidData = false;
		}
		return isValidData;
	}

	@Loggable
	@Override
	public List<String> findByTaskId(Long taskId) throws VerificationException {
		if (taskId == null) {
			throw new VerificationException("Unable find task. taskId is null.");
		}
		List<String> messages = new ArrayList<String>();
		Task task = repositoryTaskService.findFullTaskById(taskId);
		List<TripDto> tripsDto = ConvertMessageUtil.сonvertTask2TripsDto(task, false);
		if (!tripsDto.isEmpty()) {
			Map<String, List<String>> head2messages = ConvertMessageUtil.convertTripsDto2String(tripsDto, false);
			for (String head : head2messages.keySet()) {
				List<String> msgs = head2messages.get(head);
				if (msgs != null) {
					messages.addAll(msgs);
				}
			}
		}
		return messages;
	}
}
