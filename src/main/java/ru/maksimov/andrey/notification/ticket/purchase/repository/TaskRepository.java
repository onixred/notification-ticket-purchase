package ru.maksimov.andrey.notification.ticket.purchase.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;

/**
 * Интерфейс по работе c хранилищем задач
 * 
 * @author amaksimov
 */
@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {

	@Query("select t from Task t "
			+ "inner join t.criterion as c "
			+ "inner join c.trip as trip "
			+ "inner join trip.trains as trains "
			+ "inner join trains.cars as cars "
			+ "inner join cars.places as places "
			+ "where t.status = ?1 and places.dateUpdate = "
			+ "(SELECT MAX(mPlace.dateUpdate) FROM Place mPlace "
			+ "inner join mPlace.car as mCar "
			+ "inner join mCar.train as mTrain "
			+ "WHERE mTrain.id = trains.id) "
			+ " and places.dateUpdate <= ?2 and trip.date >= ?3 and trip.date <= ?4"
			+ " group by t")
	List<Task> findAllByStatusAndDate(TaskStatus status, Date dateUpdate, Date startDateOpeningTrip, Date endDateOpeningTrip);

	@Query("select t.id from Task t "
			+ "inner join t.criterion as c "
			+ "inner join c.trip as trip "
			+ "inner join trip.trains as trains "
			+ "inner join trains.cars as cars "
			+ "inner join cars.places as places "
			+ "where t.status = ?1 and places.dateUpdate = "
			+ "(SELECT MAX(mPlace.dateUpdate) FROM Place mPlace "
			+ "inner join mPlace.car as mCar "
			+ "inner join mCar.train as mTrain "
			+ "WHERE mTrain.id = trains.id) "
			+ " and places.dateUpdate <= ?2 and trip.date >= ?3 and trip.date <= ?4"
			+ " group by t")
	List<Long> findAllTaskIdByStatusAndDate(TaskStatus status, Date dateUpdate, Date startDateOpeningTrip, Date endDateOpeningTrip);

	@Query("select t from Task t "
			+ "inner join t.criterion as c "
			+ "inner join c.trip as trip "
			+ "where t.status = ?1 "
			+ " and trip.date >= ?2 and trip.date <= ?3 and t.dateUpdate < ?4 "
			+ " group by t")
	List<Task> findAllByStatusAndSendDate(TaskStatus status, Date startDateOpeningTrip, Date endDateOpeningTrip, Date lastDay);

	@Query("select t.id from Task t "
			+ "inner join t.criterion as c "
			+ "inner join c.trip as trip "
			+ "where t.status = ?1 "
			+ " and trip.date >= ?2 and trip.date <= ?3 and t.dateUpdate < ?4 "
			+ " group by t")
	List<Long> findAllTaskIdByStatusAndSendDate(TaskStatus status, Date startDateOpeningTrip, Date endDateOpeningTrip, Date lastDay);

	@Query("select t from Task t "
			+ "inner join t.user as u "
			+ "inner join t.criterion as c "
			+ "inner join c.trip as trip "
			+ "where t.status = ?1 "
			+ "and u.login =  ?2 "
			+ "group by t")
	List<Task> findAllByStatusAndLogin(TaskStatus status, String login);

	@Query("select COUNT(t) from Task t "
			+ "inner join t.user as u "
			+ "where t.status = ?1 "
			+ "and u.login =  ?2 ")
	Integer getCountTaskByStatusAndLogin(TaskStatus status, String login);

	@Query("select COUNT(t) from Task t "
			+ "where t.status = ?1 ")
	Integer getCountTaskByStatus(TaskStatus status);

	@Query("select t from Task t "
			+ "inner join t.criterion as c "
			+ "inner join c.trip as trip "
			+ "inner join trip.trains as trains "
			+ "inner join trains.cars as cars "
			+ "inner join cars.places as places "
			+ "where t.id = ?1 "
			+ "group by t")
	Task findFullTaskById(Long taskId);
}
