package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;
import ru.maksimov.andrey.notification.ticket.purchase.repository.TrainRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryTrainService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "поезд"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryTrainServiceImpl implements RepositoryTrainService {

	@Autowired
	private TrainRepository trainRepository;

	@Override
	public Train findByNumberAndStartDate(String number, Date stationStartDate) {
		return trainRepository.findByNumber(number, stationStartDate);
	}

	@Override
	public Train addTrain(Train train) {
		Train savedTrain = trainRepository.save(train);
		return savedTrain;
	}
}
