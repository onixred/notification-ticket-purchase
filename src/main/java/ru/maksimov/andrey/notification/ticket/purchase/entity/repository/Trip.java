package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;

/**
 * Сущность поездка
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "trip")
public class Trip {

	@Id
	@GeneratedValue(generator = "tripSequenceGenerator")
	@GenericGenerator(name = "tripSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "TRIP_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), 
			@Parameter(name = "increment_size", value = "1") })
	private Long id;
	@ManyToOne
	@JoinColumn(name = "station_start_id")
	private Station stationStart;
	@ManyToOne
	@JoinColumn(name = "station_end_id")
	private Station stationEnd;
	@Column(columnDefinition="date")
	private Date date;
	@JsonIgnore
	@Column(columnDefinition="Text")
	private String description;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Train> trains = new HashSet<Train>();

	@OneToMany(mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Criterion> criteria = new HashSet<Criterion>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Station getStationStart() {
		return stationStart;
	}

	public void setStationStart(Station stationStart) {
		this.stationStart = stationStart;
	}

	public Station getStationEnd() {
		return stationEnd;
	}

	public void setStationEnd(Station stationEnd) {
		this.stationEnd = stationEnd;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Train> getTrains() {
		return trains;
	}

	public void setTrains(Set<Train> trains) {
		this.trains = trains;
	}

	public Set<Criterion> getCriteria() {
		return criteria;
	}

	public void setCriteria(Set<Criterion> criteria) {
		this.criteria = criteria;
	}

	/**
	 * Сравнение без учета id и даты
	 * 
	 * @param trip
	 *            рейс
	 * @param isOverlap
	 *            признак если true допускать расхожения свойств, жесткое сравнение
	 * @return если объекты совпадают
	 */
	public boolean compare(Trip trip) {
		if (trip == this) {
			return true;
		}

		if (trip == null) {
			return false;
		}

		if (!trip.date.equals(date)) {
			return false;
		}
		if (!trip.stationStart.getId().equals(stationStart.getId())) {
			return false;
		}
		if (!trip.stationEnd.getId().equals(stationEnd.getId())) {
			return false;
		}
		return true;
	}
}