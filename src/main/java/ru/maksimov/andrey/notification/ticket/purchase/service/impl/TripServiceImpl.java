package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.config.Config;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Place;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.scheduled.ArchivingTripTask;
import ru.maksimov.andrey.notification.ticket.purchase.service.HtmlImportService;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryTripService;
import ru.maksimov.andrey.notification.ticket.purchase.service.StationService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskExecutorService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TripService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2HtmlUtil;

/**
 * Сервис по работе с сущностью "поездка"
 * 
 * @author amaksimov
 */
@Service
public class TripServiceImpl implements TripService {

	private static final Logger LOG = LogManager.getLogger(TripServiceImpl.class);

	private Config config = Config.getConfig();

	@Autowired
	private HtmlImportService htmlImportService;

	@Autowired
	private StationService stationService;

	@Autowired
	private TaskExecutorService taskExecutorService;

	@Autowired
	private RepositoryTripService repositoryTripService;

	@Autowired
	private ApplicationContext applicationContext;

	@Override
	public List<Trip> findAllByStationAndDate(Long stationStartId, Long stationEndId, Date fromDate, boolean isLoadCars)
			throws VerificationException {
		if (stationStartId == null || stationEndId == null) {
			throw new VerificationException("Unable find Stations. station is blank");
		}
		Station pointStart = stationService.findById(stationStartId);
		Station pointEnd = stationService.findById(stationEndId);
		if (pointStart == null || pointEnd == null) {
			throw new VerificationException("Unable find Stations. station is blank");
		}

		return doFindTrips(pointStart, pointEnd, fromDate, isLoadCars);
	}

	@Override
	@Loggable
	public List<Trip> findAllByStationAndDate(String nameStationStart, String nameStationEnd, Date fromDate,
			boolean isLoadCars) throws VerificationException {
		if (StringUtils.isBlank(nameStationStart) || StringUtils.isBlank(nameStationEnd)) {
			throw new VerificationException("Unable find Stations. station is blank");
		}
		nameStationStart = nameStationStart.toUpperCase();
		nameStationEnd = nameStationEnd.toUpperCase();

		Station pointStart = stationService.findOrSaveStation(nameStationStart);
		Station pointEnd = stationService.findOrSaveStation(nameStationEnd);
		if (pointStart == null || pointEnd == null) {
			throw new VerificationException("Unable find Stations. station is blank. Params: nameStationStart: "
					+ nameStationStart + ", nameStationEnd: " + nameStationStart + ", fromDate " + fromDate);
		}
		return doFindTrips(pointStart, pointEnd, fromDate, isLoadCars);
	}

	@Loggable
	@Override
	public void findOldTripAndExecutionTasks() {
		List<Long> tripIds = repositoryTripService.findAllIdByGreaterDate(new Date());
		Set<Runnable> archivingTripTasks = new HashSet<Runnable>();
		for (Long tripId : tripIds) {
			ArchivingTripTask archivingTripTask = applicationContext.getBean(ArchivingTripTask.class);
			archivingTripTask.setTripId(tripId);
			archivingTripTasks.add(archivingTripTask);
		}
		taskExecutorService.addTasks(archivingTripTasks);
	}

	@Loggable
	@Override
	public void findArchivingTripAndExecutionTasks(Long archiveTicketId) throws VerificationException {
		Set<Runnable> archivingTripTasks = new HashSet<Runnable>();
		ArchivingTripTask archivingTripTask = applicationContext.getBean(ArchivingTripTask.class);
		archivingTripTask.setArchiveTicketId(archiveTicketId);
		archivingTripTasks.add(archivingTripTask);
		taskExecutorService.addTasks(archivingTripTasks);
	}

	@Loggable
	@Override
	public void delete(long id) throws VerificationException {
		Trip trip = repositoryTripService.findById(id);
		if (trip == null) {
			throw new VerificationException("Unable delete trip. trip is null");
		}
		repositoryTripService.delete(trip);
	}

	@Loggable
	@Override
	public Trip findById(Long tripId) throws VerificationException {
		if (tripId == null) {
			throw new VerificationException("Unable find trip. tripId is null");
		}
		return repositoryTripService.findById(tripId);
	}

	@Override
	public void delete(List<Long> ids) throws VerificationException {
		if (ids == null) {
			throw new VerificationException("Unable delete list trip. List trip is null");
		}
		repositoryTripService.delete(ids);
	}

	/**
	 * Поиск рейсов, с сохранение в хранилище
	 * 
	 * @param pointStart
	 *            станция отправления
	 * @param pointEnd
	 *            станция прибытия
	 * @param fromDate
	 *            дата отправки
	 * @param isLoadCars
	 *            признак обновления данных о местах и вагонах
	 * @return рейс сохраненный в хранилище
	 */
	private List<Trip> doFindTrips(Station pointStart, Station pointEnd, Date fromDate, boolean isLoadCars)
			throws VerificationException {
		if (pointStart == null || pointEnd == null) {
			throw new VerificationException("Unable find Stations. station is blank");
		}

		List<Trip> trips = repositoryTripService.findByStationAndDate(pointStart, pointEnd, fromDate);
		if (trips.isEmpty()) {
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station stationStart = new ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station();
			stationStart.setName(pointStart.getName());
			stationStart.setCode(pointStart.getCode());
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station stationEnd = new ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station();
			stationEnd.setName(pointEnd.getName());
			stationEnd.setCode(pointEnd.getCode());
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Trip[] htmlTrips = htmlImportService
					.findAllByStationAndDate(stationStart, stationEnd, fromDate);
			for (ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Trip htmlTrip : htmlTrips) {
				Trip trip = ConvertEntity2HtmlUtil.convertTripHtml2Repository(htmlTrip, null);
				// Запрос на сайт для получения информации о вагонах и местах
				// для каждого поезда
				fillTrains(trip, false);
				try {
					trips.add(findAndSaveTrip(trip));
				} catch (DataAccessException dae) {
					LOG.warn("Unable save 'Trip' " + trip + " Details:" + dae.getMessage());
				}
			}
		} else if (isLoadCars && !checkRelevanceDate(trips)) {
			List<Trip> updateTrips = new ArrayList<Trip>();
			for (Trip trip : trips) {
				fillTrains(trip, false);
				try {
					updateTrips.add(findAndSaveTrip(trip));
				} catch (DataAccessException dae) {
					LOG.warn("Unable save 'Trip' " + trip + " Details:" + dae.getMessage());

				}
			}
			trips = updateTrips;
		}
		return trips;
	}

	/**
	 * Обновить/Дополнить рейс на основе сайта Добавляються вагоны и места в
	 * поезде
	 * 
	 * @param trip
	 *            рейс в формате хранилища
	 * @param isUpdateTrains
	 *            признак обновление пустого вагонов (это значит рекурсивный
	 *            вызов что бы не зациклица)
	 * @return рейс сохраненный в хранилище
	 */
	private void fillTrains(Trip trip, boolean isUpdateTrains) {
		if (!trip.getTrains().isEmpty()) {
			for (Train train : trip.getTrains()) {
				Set<Car> cars = new HashSet<Car>();
				ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station htmlPointStart = ConvertEntity2HtmlUtil
						.convertStationRepository2Html(trip.getStationStart());
				ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station htmlPointEnd = ConvertEntity2HtmlUtil
						.convertStationRepository2Html(trip.getStationEnd());
				ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Train htmlTrain = htmlImportService
						.findByStationAndDateAndTrainNumber(htmlPointStart, htmlPointEnd, trip.getDate(),
								train.getNumber());
				if (htmlTrain.getCars() != null) {
					for (ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Car htmlCar : htmlTrain
							.getCars()) {
						Car car = ConvertEntity2HtmlUtil.convertCarHtml2Repository(htmlCar, train);
						cars.add(car);
					}
					if (train.getCars() == null) {
						train.setCars(cars);
					} else {
						train.getCars().addAll(cars);
					}
				}
			}
		} else {
			if (!isUpdateTrains) {
				ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station stationStart = new ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station();
				stationStart.setName(trip.getStationStart().getName());
				stationStart.setCode(trip.getStationStart().getCode());
				ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station stationEnd = new ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station();
				stationEnd.setName(trip.getStationEnd().getName());
				stationEnd.setCode(trip.getStationEnd().getCode());
				ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Trip[] htmlTrips = htmlImportService
						.findAllByStationAndDate(stationStart, stationEnd, trip.getDate());
				if (htmlTrips != null && htmlTrips.length > 0) {

					Trip newTrip = ConvertEntity2HtmlUtil.convertTripHtml2Repository(htmlTrips[0], trip.getId());
					trip.getTrains().addAll(newTrip.getTrains());
					fillTrains(trip, true);
				}
			}

		}
	}

	/**
	 * Найти для рейса станции или сохранить их, и затем сохранить сам рейс
	 * 
	 * @param trip
	 *            рейс в формате хранилища
	 * @return рейс сохраненный в хранилище
	 */
	private Trip findAndSaveTrip(Trip trip) throws VerificationException {
		Station stationStart = trip.getStationStart();
		if (stationStart != null) {
			stationStart = stationService.findOrSaveStation(stationStart);
			trip.setStationStart(stationStart);
		}
		Station stationEnd = trip.getStationEnd();
		if (stationEnd != null) {
			stationEnd = stationService.findOrSaveStation(stationEnd);
			trip.setStationEnd(stationEnd);
		}
		trip = repositoryTripService.addTrip(trip);
		return trip;
	}

	/**
	 * Проверка даты обновления билетов
	 * 
	 * @param trip
	 *            рейс в формате хранилища
	 * @return признак равен trur если данные обновлены иначе false
	 */
	private boolean checkRelevanceDate(List<Trip> trips) {
		for (Trip trip : trips) {
			if (trip.getTrains().isEmpty()) {
				// нужно обновлять
				return false;
			}
			for (Train train : trip.getTrains()) {
				for (Car car : train.getCars()) {
					boolean isRelevancePlace = false;
					LOG.debug("Detale train " + train.getNumber() + " car: " + car.getName() + " place: "
							+ car.getPlaceNumbers() + " type " + car.getType());
					for (Place place : car.getPlaces()) {
						if (сheckPeriod(place.getDateUpdate())) {
							isRelevancePlace = true;
							break;
						}
					}

					if (isRelevancePlace) {
						// в этом вагоне данные обновлены. вагоны тоже обновлены
						break;
					} else {
						// нужно обновлять
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Проверка даты меньше ли она теучей на задоное количествор секунд.
	 * 
	 * @param trip
	 *            рейс в формате хранилища
	 * @return признак равен trur если разность даты и текущей даты меньше чем
	 *         задано в конфиге иначе false
	 */
	private boolean сheckPeriod(Date date) {
		boolean isRelevanceDate = true;
		Calendar cal = Calendar.getInstance();
		long diffDateInMin = TimeUnit.MILLISECONDS.toMinutes(Math.abs(cal.getTimeInMillis() - date.getTime()));
		LOG.debug("Check period: " + diffDateInMin / 60 + " h");
		if (diffDateInMin > config.getDateReelevancePeriodMin()) {
			isRelevanceDate = false;
		}
		return isRelevanceDate;
	}
}
