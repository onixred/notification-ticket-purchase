package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;

/**
 * Сущность архив свободных мест для хранилищя
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "archive_free_placet")
public class ArchiveFreePlace {

	@Id
	@GeneratedValue(generator = "archiveFreePlacetSequenceGenerator")
	@GenericGenerator(name = "archiveFreePlacetSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "ARCHIVE_FREE_PLACET_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1") })
	private Long id;

	@Enumerated(EnumType.STRING)
	private PlaceType placeType;
	private int freeNumber;
	private double tariff;
	private double tariffAlternative;
	private Date dateUpdate;

	@ManyToOne
	@JoinColumn(name = "archive_ticket_id")
	private ArchiveTicket archiveTicket;

	public Long getId() {
		return id;
	}

	public int getFreeNumber() {
		return freeNumber;
	}

	public double getTariff() {
		return tariff;
	}

	public double getTariffAlternative() {
		return tariffAlternative;
	}

	public Date getDateUpdate() {
		return dateUpdate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFreeNumber(int freeNumber) {
		this.freeNumber = freeNumber;
	}

	public void setTariff(double tariff) {
		this.tariff = tariff;
	}

	public void setTariffAlternative(double tariffAlternative) {
		this.tariffAlternative = tariffAlternative;
	}

	public void setDateUpdate(Date dateUpdate) {
		this.dateUpdate = dateUpdate;
	}

	public PlaceType getPlaceType() {
		return placeType;
	}

	public void setPlaceType(PlaceType placeType) {
		this.placeType = placeType;
	}

	public ArchiveTicket getArchiveTicket() {
		return archiveTicket;
	}

	public void setArchiveTicket(ArchiveTicket archiveTicket) {
		this.archiveTicket = archiveTicket;
	}

}
