package ru.maksimov.andrey.notification.ticket.purchase.service;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;

import java.util.List;

import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "задача"
 * 
 * @author amaksimov
 */
public interface TaskService {

	/**
	 * Добавить задачу
	 * 
	 * @param taskDto
	 *            форма задачи
	 * @return задача
	 */
	TaskDto addTask(TaskDto taskDto) throws VerificationException;

	/**
	 * Добавить\обновить задачу
	 * 
	 * @param task
	 *            задача
	 * @return задача
	 */
	Task addTask(Task task);

	/**
	 * Найти все новые актуальные задачи и добавить их в обработчик
	 */
	void findAllNewAndExecutionTasks();

	/**
	 * Найти задачу в хранилище по id.
	 * 
	 * @param taskId
	 *            id задачи
	 * @return задача
	 */
	Task doFindById(Long taskId);

	/**
	 * Найти все задачи по состоянию и логину
	 * 
	 * @param status
	 *            статус задачи
	 * @param login
	 *            логин пользователя
	 * @return список задач
	 */
	List<TaskDto> findAllByStatusAndLogin(TaskStatus status, String login) throws VerificationException;

	/**
	 * Получить количество задач по состоянию и логину 
	 * 
	 * @param status
	 *            статус задачи
	 * @param login
	 *            логин пользователя
	 * @return число задач
	 */
	Integer getCountTaskByStatusAndLogin(TaskStatus status, String login) throws VerificationException;

	/**
	 * Получить количество задач по состоянию
	 * 
	 * @param status
	 *            статус задачи
	 * @return число задач
	 */
	Integer getCountTaskByStatus(TaskStatus status) throws VerificationException;

	/**
	 * Найти задачу в хранилище по id.
	 * 
	 * @param taskId
	 *            id задачи
	 * @return задача
	 */
	TaskDto findById(Long taskId) throws VerificationException;

	/**
	 * Найти сообщения по идентификатору задачи.
	 * 
	 * @param taskId
	 *            id задачи
	 * @return список сообщений
	 */
	List<String> findByTaskId(Long taskId) throws VerificationException;
}
