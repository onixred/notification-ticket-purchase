package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;
import ru.maksimov.andrey.notification.ticket.purchase.repository.TimetableRepository;
import ru.maksimov.andrey.notification.ticket.purchase.repository.UserRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryUserService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "пользователь"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryUserServiceImpl implements RepositoryUserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TimetableRepository timetableRepository;

	@Override
	public User addUser(User user) {
		User savedUser = userRepository.save(user);
		return savedUser;
	}

	@Override
	public User findByLogin(String login) {
		User user = userRepository.findByLogin(login);
		return user;
	}

	@Override
	public List<Timetable> findAllTimetableByLogin(String login) {
		List<Timetable> timetables = timetableRepository.findAllTimetableByLogin(login);
		return timetables;
	}

	@Override
	public List<Timetable> findAllTimetableUserId(Long id) {
		List<Timetable> timetables = timetableRepository.findAllTimetableUserId(id);
		return timetables;
	}

	@Override
	public Timetable addTimetable(Timetable timetable) {
		Timetable savedTimetable = timetableRepository.save(timetable);
		return savedTimetable;
	}

	@Override
	public void deleteTimetable(Long id) {
		timetableRepository.delete(id);
	}

	@Override
	public Integer getCountTimetableByLogin(String login) {
		return timetableRepository.getCountTimetableByLogin(login);
	}

}
