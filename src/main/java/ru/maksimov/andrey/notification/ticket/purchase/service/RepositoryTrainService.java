package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Date;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "Поезд"
 * 
 * @author amaksimov
 */
public interface RepositoryTrainService {

	/**
	 * Найти поезд в хранилище по дате оправления и номеру
	 * 
	 * @param number
	 *            номер поезда
	 * @param stationStartDate
	 *            дате оправления со станции
	 * @return поезд
	 */
	Train findByNumberAndStartDate(String number, Date stationStartDate);

	/**
	 * Добавить\обновить поезд в хранилище
	 * 
	 * @param train
	 *            поезд
	 * @return структура поезд
	 */
	Train addTrain(Train train);

}
