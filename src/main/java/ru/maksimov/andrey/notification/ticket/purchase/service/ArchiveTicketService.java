package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "арив билетов"
 * 
 * @author amaksimov
 */
public interface ArchiveTicketService {

	/**
	 * Добавить\обновить аривный билет
	 * 
	 * @param archiveTicket
	 *            архивный билет
	 * @return билет
	 */
	ArchiveTicket addArchiveTicket(ArchiveTicket archiveTicket) throws VerificationException;

	/**
	 * Добавить\обновить список аривных билетов
	 * 
	 * @param archiveTickets
	 *            список архивных билетов
	 * @return список билетов
	 */
	List<ArchiveTicket> addArchiveTickets(List<ArchiveTicket> archiveTickets) throws VerificationException;

	/**
	 * Найти все архивные билеты по списку идентификаторов
	 * 
	 * @param archiveTicketIds
	 *            список идентификаторов
	 * @return список архивных билетов
	 */
	List<ArchiveTicket> findAllById(List<Long> archiveTicketIds) throws VerificationException;

	/**
	 * Найти архивный билет по идентификатору
	 * 
	 * @param archiveTicketId
	 *            идентификатор архивного билета
	 * @return архив билета
	 */
	ArchiveTicket findById(Long archiveTicketId) throws VerificationException;

}
