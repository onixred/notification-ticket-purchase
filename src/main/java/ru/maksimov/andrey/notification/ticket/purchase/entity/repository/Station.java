package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Сущность город (станция)
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "station")
public class Station {

	@Id
	@GeneratedValue(generator = "stationSequenceGenerator")
	@GenericGenerator(name = "stationSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "STATION_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), 
			@Parameter(name = "increment_size", value = "1") })
	private Long id;
	@Column(unique = true, nullable = false, length = 150)
	private String name;
	@Column(unique = true)
	private long code;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}
}
