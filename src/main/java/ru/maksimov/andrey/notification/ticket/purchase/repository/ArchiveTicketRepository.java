package ru.maksimov.andrey.notification.ticket.purchase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;

/**
 * Интерфейс по работе c хранилищем арив билетов
 * 
 * @author amaksimov
 */
@Repository
public interface ArchiveTicketRepository extends CrudRepository<ArchiveTicket, Long> {

}
