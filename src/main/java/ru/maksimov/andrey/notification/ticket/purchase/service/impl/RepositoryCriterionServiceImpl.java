package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.notification.ticket.purchase.repository.CriterionRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryCriterionService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "критерии задачи"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryCriterionServiceImpl implements RepositoryCriterionService {

	@Autowired
	private CriterionRepository criterionRepository;

	@Override
	public Criterion addСriterion(Criterion сriterion) {
		Criterion savedСriterion = criterionRepository.save(сriterion);
		return savedСriterion;
	}

}
