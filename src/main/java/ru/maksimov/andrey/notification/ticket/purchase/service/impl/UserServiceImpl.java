package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.UserDto;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryUserService;
import ru.maksimov.andrey.notification.ticket.purchase.service.UserService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2DtoUtil;

/**
 * Сервис по работе с сущностью "пользователь"
 * 
 * @author amaksimov
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private RepositoryUserService repositoryUserService;

	@Override
	public UserDto findByLogin(String login) throws VerificationException {
		User user = doFindByLogin(login);
		UserDto гserDto = ConvertEntity2DtoUtil.convertUser2UserDto(user);
		return гserDto;
	}

	@Override
	public User doFindByLogin(String login) throws VerificationException {

		if (login == null || StringUtils.isBlank(login)) {
			throw new VerificationException("Unable find User. login is blank");
		}
		User saveUser = repositoryUserService.findByLogin(login);
		if (saveUser == null) {
			throw new VerificationException("Unable find User where login " + login);
		}
		return saveUser;
	}

	@Override
	public UserDto addUser(UserDto userDto) throws VerificationException {
		if (userDto.getLogin() == null || StringUtils.isBlank(userDto.getLogin())) {
			throw new VerificationException("Unable add User. login is blank");
		}
		User user = ConvertEntity2DtoUtil.convertUserDto2User(userDto);
		User saveUser = repositoryUserService.findByLogin(userDto.getLogin());
		if (saveUser != null) {
			throw new VerificationException("Unable save User where login " + userDto.getLogin());
		}
		repositoryUserService.addUser(user);
		return userDto;
	}

	@Override
	public List<TimetableDto> findAllTimetableByLogin(String login) throws VerificationException {
		if (login == null || StringUtils.isBlank(login)) {
			throw new VerificationException("Unable find all Timetable. login is blank");
		}
		List<Timetable> timetables = repositoryUserService.findAllTimetableByLogin(login);
		List<TimetableDto> timetablesDto = new ArrayList<TimetableDto>();
		for (Timetable timetable : timetables) {
			TimetableDto timetableDto = ConvertEntity2DtoUtil.convertTimetable2TimetableDto(timetable);
			timetablesDto.add(timetableDto);
		}
		return timetablesDto;
	}

	@Override
	public List<TimetableDto> findAllTimetableByUserId(Long id) throws VerificationException {
		
		List<Timetable> timetables = doFindAllTimetableByUserId(id);
		List<TimetableDto> timetablesDto = new ArrayList<TimetableDto>();
		for (Timetable timetable : timetables) {
			TimetableDto timetableDto = ConvertEntity2DtoUtil.convertTimetable2TimetableDto(timetable);
			timetablesDto.add(timetableDto);
		}
		return timetablesDto;
	}

	@Override
	public List<Timetable> doFindAllTimetableByUserId(Long id) throws VerificationException {
		if (id == null) {
			throw new VerificationException("Unable find all Timetable. id is blank");
		}
		return repositoryUserService.findAllTimetableUserId(id);
	}


	@Override
	public TimetableDto updateTimetable(TimetableDto tеimetableDto, String login) throws VerificationException {
		User user = doFindByLogin(login);
		if (tеimetableDto == null) {
			throw new VerificationException("Unable find all Timetable. login is blank");
		}
		Timetable timetable = ConvertEntity2DtoUtil.convertTimetable2TimetableDto(tеimetableDto);
		timetable.setUser(user);
		Timetable saveTimetable = repositoryUserService.addTimetable(timetable);
		TimetableDto saveTimetableDto = ConvertEntity2DtoUtil.convertTimetable2TimetableDto(saveTimetable);
		return saveTimetableDto;
	}

	@Override
	public void deleteTimetable(Long id) throws VerificationException {
		if (id == null) {
			throw new VerificationException("Unable deleted Timetable. id is blank");
		}
		repositoryUserService.deleteTimetable(id);
	}

	@Override
	public Integer getCountTimetableByLogin(String login) throws VerificationException {
		if (StringUtils.isBlank(login)) {
			throw new VerificationException("Unable get count Timetable. login is blank");
		}
		return repositoryUserService.getCountTimetableByLogin(login);
	}
}
