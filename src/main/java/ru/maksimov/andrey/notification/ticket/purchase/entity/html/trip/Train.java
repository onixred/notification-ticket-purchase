package ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Сущность поезд РЖД
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Train {

	private String number;
	private String numberAlternative;
	private long type;
	private Date startDate;
	private String startTime;
	private Date endDate;
	private String endTime;

	@JsonProperty("number")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@JsonProperty("number2")
	public String getNumberAlternative() {
		return numberAlternative;
	}

	public void setNumberAlternative(String numberAlternative) {
		this.numberAlternative = numberAlternative;
	}

	@JsonProperty("type")
	public long getType() {
		return type;
	}

	public void setType(long type) {
		this.type = type;
	}

	@JsonProperty("date0")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateUtils.SHORT_DATE_FORMAT_RUSSIA, timezone = "CET")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@JsonProperty("time0")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@JsonProperty("date1")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateUtils.SHORT_DATE_FORMAT_RUSSIA, timezone = "CET")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@JsonProperty("time1")
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
