
package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;

/**
 * Сущность вагона для хранилищя
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "car")
public class Car {

	@Id
	@GeneratedValue(generator = "carSequenceGenerator")
	@GenericGenerator(name = "carSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "CAR_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), 
			@Parameter(name = "increment_size", value = "1") })
	private Long id;
	private String name;
	private String classType;
	@Enumerated(EnumType.STRING)
	private CarType type;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "car", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Place> places = new HashSet<Place>();
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "train_id")
	private Train train;
	private String placeNumbers;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	public CarType getType() {
		return type;
	}

	public void setType(CarType type) {
		this.type = type;
	}

	public Set<Place> getPlaces() {
		return places;
	}

	public void setPlaces(Set<Place> places) {
		this.places = places;
	}

	public Train getTrain() {
		return train;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public String getPlaceNumbers() {
		return placeNumbers;
	}

	public void setPlaceNumbers(String placeNumbers) {
		this.placeNumbers = placeNumbers;
	}
}
