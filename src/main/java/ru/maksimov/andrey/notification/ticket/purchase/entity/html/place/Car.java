package ru.maksimov.andrey.notification.ticket.purchase.entity.html.place;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Сущность вагон РЖД
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car {

	private String countNumber;
	private String typeLoc;
	private String classType;
	private Double tariff;
	private Double tariffAlternative;
	private Double tariffServ;
	private Seat[] seats;
	private String places;

	@JsonProperty("cnumber")
	public String getCountNumber() {
		return countNumber;
	}

	public void setCountNumber(String countNumber) {
		this.countNumber = countNumber;
	}

	@JsonProperty("typeLoc")
	public String getTypeLoc() {
		return typeLoc;
	}

	public void setTypeLoc(String typeLoc) {
		this.typeLoc = typeLoc;
	}

	@JsonProperty("clsType")
	public String getClassType() {
		return classType;
	}

	public void setClassType(String classType) {
		this.classType = classType;
	}

	@JsonProperty("tariff")
	public Double getTariff() {
		return tariff;
	}

	public void setTariff(Double tariff) {
		this.tariff = tariff;
	}

	@JsonProperty("tariff2")
	public Double getTariffAlternative() {
		return tariffAlternative;
	}

	public void setTariffAlternative(Double tariffAlternative) {
		this.tariffAlternative = tariffAlternative;
	}

	@JsonProperty("tariffServ")
	public Double getTariffServ() {
		return tariffServ;
	}

	public void setTariffServ(Double tariffServ) {
		this.tariffServ = tariffServ;
	}

	@JsonProperty("seats")
	public Seat[] getSeats() {
		return seats;
	}

	public void setSeats(Seat[] seats) {
		this.seats = seats;
	}

	@JsonProperty("places")
	public String getPlaces() {
		return places;
	}

	public void setPlaces(String places) {
		this.places = places;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
