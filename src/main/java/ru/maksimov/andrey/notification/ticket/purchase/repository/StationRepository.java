package ru.maksimov.andrey.notification.ticket.purchase.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;

/**
 * Интерфейс по работе c хранилищем городами
 * 
 * @author amaksimov
 */
@Repository
public interface StationRepository extends CrudRepository<Station, Long> {

	@Query("FROM Station c where c.name LIKE ?1%")
	List<Station> findAllByNamePart(String name);

	@Query("FROM Station c where c.code = ?1")
	Station findByCode(long code);

	@Query("FROM Station c where c.name = ?1")
	Station findByName(String name);
}
