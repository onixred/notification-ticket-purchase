package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;
import ru.maksimov.andrey.notification.ticket.purchase.repository.ArchiveTicketRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryArchiveTicketService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "арив билетов"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryArchiveTicketServiceImpl implements RepositoryArchiveTicketService {

	@Autowired
	private ArchiveTicketRepository archiveTicketRepository;

	@Override
	public ArchiveTicket addArchiveTicket(ArchiveTicket archiveTicket) {
		ArchiveTicket savedArchiveTicket = archiveTicketRepository.save(archiveTicket);
		return savedArchiveTicket;
	}

	@Override
	public List<ArchiveTicket> addArchiveTickets(List<ArchiveTicket> archiveTickets) {
		List<ArchiveTicket> list = new ArrayList<ArchiveTicket>();
		Iterable<ArchiveTicket> savedArchiveTicket = archiveTicketRepository.save(archiveTickets);
		for (ArchiveTicket archiveTicket : savedArchiveTicket) {
			list.add(archiveTicket);
		}
		return list;
	}

	@Override
	public List<ArchiveTicket> findAllById(List<Long> archiveTicketIds) {
		List<ArchiveTicket> list = new ArrayList<ArchiveTicket>();
		Iterable<ArchiveTicket> savedArchiveTicket = archiveTicketRepository.findAll(archiveTicketIds);
		for (ArchiveTicket archiveTicket : savedArchiveTicket) {
			list.add(archiveTicket);
		}
		return list;
	}

	@Override
	public ArchiveTicket findById(Long archiveTicketId) {
		return archiveTicketRepository.findOne(archiveTicketId);
	}
}
