package ru.maksimov.andrey.notification.ticket.purchase.service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "критерии задачи"
 * 
 * @author amaksimov
 */
public interface RepositoryCriterionService {

	/**
	 * Добавить\обновить критерии для задачи
	 * 
	 * @param сriterion
	 *            критерии
	 * @return структура станция
	 */
	Criterion addСriterion(Criterion сriterion);

}
