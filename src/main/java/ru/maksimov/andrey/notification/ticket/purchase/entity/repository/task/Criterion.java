package ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;

/**
 * Сущность критерии задачи
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "criterion")
public class Criterion {

	@Id
	@GeneratedValue(generator = "criterionSequenceGenerator")
	@GenericGenerator(name = "criterionSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "CRITERION_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1") })
	private Long id;
	@Enumerated(EnumType.STRING)
	private PlaceType placeType;
	private int countPlace;
	@Enumerated(EnumType.STRING)
	private CarType carType;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "trip_id")
	private Trip trip;

	@OneToMany(mappedBy = "criterion", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Task> tasks = new HashSet<Task>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlaceType getPlaceType() {
		return placeType;
	}

	public void setPlaceType(PlaceType placeType) {
		this.placeType = placeType;
	}

	public int getCountPlace() {
		return countPlace;
	}

	public void setCountPlace(int countPlace) {
		this.countPlace = countPlace;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}

	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	public Set<Task> getTasks() {
		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	/**
	 * Сравнение без учета id и даты
	 * 
	 * @param criterion
	 *            критерии задач
	 * @param isOverlap
	 *            признак если true допускать расхожения свойств, жесткое сравнение
	 * @return если объекты совпадают
	 */
	public boolean compare(Criterion criterion, boolean isOverlap) {
		if (criterion == this) {
			return true;
		}

		if (criterion == null) {
			return false;
		}

		if (!criterion.placeType.equals(placeType) && (!criterion.placeType.equals(PlaceType.OTHER) || !isOverlap)) {
			return false;
		}

		if (criterion.countPlace != countPlace) {
			return false;
		}

		if (!criterion.carType.equals(carType) && (!criterion.carType.equals(CarType.OTHER)|| !isOverlap)) {
			return false;
		}

		if (!criterion.trip.compare(trip)) {
			return false;
		}

		return true;
	}
}
