package ru.maksimov.andrey.notification.ticket.purchase.entity.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Сущность поезд
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "train")
public class Train {

	@Id
	@GeneratedValue(generator = "trainSequenceGenerator")
	@GenericGenerator(name = "trainSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "TRAIN_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), 
			@Parameter(name = "increment_size", value = "1") })
	private Long id;
	private String number;
	private String numberAlternative;
	private String type;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "train", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Car> cars = new HashSet<Car>();
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "trip_id")
	private Trip trip;
	private Date stationStartDate;
	private Date stationEndDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public void setNumberAlternative(String numberAlternative) {
		this.numberAlternative = numberAlternative;
	}

	public String getNumberAlternative() {
		return numberAlternative;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<Car> getCars() {
		return cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}


	public Trip getTrip() {
		return trip;
	}

	public void setTrip(Trip trip) {
		this.trip = trip;
	}

	public Date getStationStartDate() {
		return stationStartDate;
	}

	public void setStationStartDate(Date stationStartDate) {
		this.stationStartDate = stationStartDate;
	}

	public Date getStationEndDate() {
		return stationEndDate;
	}

	public void setStationEndDate(Date stationEndDate) {
		this.stationEndDate = stationEndDate;
	}

}
