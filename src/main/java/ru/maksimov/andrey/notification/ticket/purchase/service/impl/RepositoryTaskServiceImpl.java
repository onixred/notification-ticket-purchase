package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;
import ru.maksimov.andrey.notification.ticket.purchase.repository.TaskRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryTaskService;
import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Реализация сервиса по работе c хранилищем, сущность "задача"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryTaskServiceImpl implements RepositoryTaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Loggable
	@Override
	public List<Task> findAllByStatusAndDate(TaskStatus status, int updateIntervalMin, int openingPeriodTrip, Date date) {
		Date dateUpdate = DateUtils.addDate(date, Calendar.MINUTE, -updateIntervalMin);
		Date endDateOpeningTrip = DateUtils.addDate(date, Calendar.DATE, openingPeriodTrip);
		List<Task> tasks = taskRepository.findAllByStatusAndDate(status, dateUpdate, date, endDateOpeningTrip);
		return tasks;
	}

	@Loggable
	@Override
	public List<Long> findAllTaskIdByStatusAndDate(TaskStatus status, int updateIntervalMin, int openingPeriodTrip, Date date) {
		Date dateUpdate = DateUtils.addDate(date, Calendar.MINUTE, -updateIntervalMin);
		Date endDateOpeningTrip = DateUtils.addDate(date, Calendar.DATE, openingPeriodTrip);
		List<Long> taskIds = taskRepository.findAllTaskIdByStatusAndDate(status, dateUpdate, date, endDateOpeningTrip);
		return taskIds;
	}

	@Loggable
	@Override
	public List<Task> findAllByStatusAndDate(TaskStatus status, int openingPeriodTrip, Date date) {
		Date endDateOpeningTrip = DateUtils.addDate(date, Calendar.DATE, openingPeriodTrip);
		Date lastDay = DateUtils.addDate(date, Calendar.DATE, -1);
		List<Task> tasks = taskRepository.findAllByStatusAndSendDate(status, date, endDateOpeningTrip, lastDay);
		return tasks;
	}

	@Loggable
	@Override
	public List<Long> findAllTaskIdByStatusAndDate(TaskStatus status, int openingPeriodTrip, Date date) {
		Date endDateOpeningTrip = DateUtils.addDate(date, Calendar.DATE, openingPeriodTrip);
		Date lastDay = DateUtils.addDate(date, Calendar.DATE, -1);
		List<Long> taskIds = taskRepository.findAllTaskIdByStatusAndSendDate(status, date, endDateOpeningTrip, lastDay);
		return taskIds;
	}

	@Override
	public Task findById(Long id) {
		return taskRepository.findOne(id);
	}

	@Override
	public Task addTask(Task task) {
		return taskRepository.save(task);
	}

	@Override
	public List<Task> findAllByStatusAndLogin(TaskStatus status, String login) {
		return taskRepository.findAllByStatusAndLogin(status, login);
	}

	@Override
	public Integer getCountTaskByStatusAndLogin(TaskStatus status, String login) {
		return taskRepository.getCountTaskByStatusAndLogin(status, login);
	}

	@Override
	public Integer getCountTaskByStatus(TaskStatus status) {
		return taskRepository.getCountTaskByStatus(status);
	}

	@Loggable
	@Override
	public Task findFullTaskById(Long taskId) {
		Task task = taskRepository.findFullTaskById(taskId);
		return task;
	}
}
