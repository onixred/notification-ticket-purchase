package ru.maksimov.andrey.notification.ticket.purchase.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TripService;

/**
 * Диспечер задач
 * 
 * @author amaksimov
 */
@Component
public class ScheduledTasks {

	@Autowired
	private TaskService taskService;

	@Autowired
	private TripService tripService;

	@Loggable
	@Scheduled(fixedRate = 100000)
	public void findNewAndExecutionTasks() {
		taskService.findAllNewAndExecutionTasks();
	}

	@Loggable
	@Scheduled(cron = "0 1 1 * * ?")
	public void findOldAndExecutionTasks() {
		tripService.findOldTripAndExecutionTasks();
	}
}
