package ru.maksimov.andrey.notification.ticket.purchase.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ContactDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.StationDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.UserDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.ContactType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveFreePlace;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Contact;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;

/**
 * Вспомогаткельный класс для конвертации сущностей и dto
 * 
 * @author amaksimov
 */
public class ConvertEntity2DtoUtil {

	private static final Logger LOG = LogManager.getLogger(ConvertEntity2DtoUtil.class);

	/**
	 * Конвертация из DTO задачи в "поездку" в формате хранилища
	 * 
	 * @param taskDto
	 *            форма задачи
	 * @return рейс
	 */
	public static Trip convertTaskDto2Trip(TaskDto taskDto, User user) {
		Trip repositoryTrip = new Trip();
		Station stationStart = ConvertEntity2HtmlUtil.createRepositoryStation(taskDto.getStationStartId());
		repositoryTrip.setStationStart(stationStart);
		Station stationEnd = ConvertEntity2HtmlUtil.createRepositoryStation(taskDto.getStationEndId());
		repositoryTrip.setStationEnd(stationEnd);
		repositoryTrip.setDate(taskDto.getStartDate());
		Criterion criterion = convertTaskDto2Сriterion(taskDto, repositoryTrip, user);
		Set<Criterion> criteria = Collections.singleton(criterion);
		repositoryTrip.setCriteria(criteria);
		return repositoryTrip;
	}

	/**
	 * Конвертация из DTO задачи в "критерий задачи" в формате хранилища
	 * 
	 * @param taskDto
	 *            форма задачи
	 * @param trip
	 *            поездка
	 * @return сущность критерий задачи
	 */
	public static Criterion convertTaskDto2Сriterion(TaskDto taskDto, Trip trip, User user) {
		Criterion repositoryСriterion = new Criterion();
		repositoryСriterion.setCountPlace(taskDto.getCountPlace());
		Task task = convertTaskDto2Task(taskDto, repositoryСriterion, user);
		Set<Task> tasks = Collections.singleton(task);
		repositoryСriterion.setTasks(tasks);
		repositoryСriterion.setTrip(trip);
		CarType carType = ConvertEntity2HtmlUtil.createCarType(taskDto.getCarType());
		repositoryСriterion.setCarType(carType);
		PlaceType placeType = ConvertEntity2HtmlUtil.createSeatType(taskDto.getPlaceType());
		repositoryСriterion.setPlaceType(placeType);
		return repositoryСriterion;
	}

	/**
	 * Конвертация из DTO задачи в сущность "задачу" в формате хранилища
	 * 
	 * @param taskDto
	 *            форма задачи
	 * @param сriterion
	 *            критерии задачи
	 * @return сущность задача
	 */
	public static Task convertTaskDto2Task(TaskDto taskDto, Criterion сriterion, User user) {
		Task repositoryTask = new Task();
		repositoryTask.setCriterion(сriterion);
		repositoryTask.setDateUpdate(new Date());
		repositoryTask.setStatus(TaskStatus.NEW);
		repositoryTask.setUser(user);
		return repositoryTask;
	}

	/**
	 * Конвертация из DTO пользователь в сущность "пользователь"
	 * 
	 * @param userDto
	 *            форма пользователь
	 * @param сriterion
	 *            критерии задачи
	 * @return сущность задача
	 */
	public static User convertUserDto2User(UserDto userDto) {
		User user = new User();
		Set<Contact> сontacts = new HashSet<Contact>();
		for (ContactDto contactDto : userDto.getContacts()) {
			Contact contact = convertContactDto2Contact(contactDto);
			contact.setUser(user);
			сontacts.add(contact);
		}
		user.setContacts(сontacts);
		user.setLogin(userDto.getLogin());

		return user;
	}

	/**
	 * Конвертация из сущности пользователь в DTO "пользователь"
	 * 
	 * @param user
	 *            пользователь
	 * @param сriterion
	 *            критерии задачи
	 * @return сущность задача
	 */
	public static UserDto convertUser2UserDto(User user) {
		UserDto userDto = new UserDto();
		Set<ContactDto> сontactsDto = new HashSet<ContactDto>();
		for (Contact contact : user.getContacts()) {
			ContactDto contactDto = convertContact2ContactDto(contact);
			сontactsDto.add(contactDto);
		}
		userDto.setContacts(сontactsDto);
		userDto.setLogin(user.getLogin());
		return userDto;
	}

	/**
	 * Конвертация из сущности "задача" в DTO задачи
	 * 
	 * @param saveTask
	 *            форма задачи
	 * @return DTO задача
	 */
	public static TaskDto convertTask2TaskDto(Task saveTask) {
		TaskDto taskDto = new TaskDto();
		Criterion сriterion = saveTask.getCriterion();
		Trip trip = сriterion.getTrip();
		User user = saveTask.getUser();
		taskDto.setTaskId(saveTask.getId());
		taskDto.setCountPlace(сriterion.getCountPlace());
		taskDto.setStationStartId(trip.getStationStart().getId());
		taskDto.setStationStartName(trip.getStationStart().getName());
		taskDto.setStationEndId(trip.getStationEnd().getId());
		taskDto.setStationEndName(trip.getStationEnd().getName());
		taskDto.setLogin(user.getLogin());
		taskDto.setStartDate(trip.getDate());
		taskDto.setCarType(сriterion.getCarType().getName());
		taskDto.setPlaceType(сriterion.getPlaceType().getMessage());
		return taskDto;
	}

	/**
	 * Конвертация из сущности контакта в DTO контак
	 * 
	 * @param contact
	 *            контакт
	 * @return DTO контакт
	 */
	private static ContactDto convertContact2ContactDto(Contact contact) {
		ContactDto contactDto = new ContactDto();
		contactDto.setContact(contact.getContact());
		contactDto.setType(contact.getType());
		return contactDto;
	}

	/**
	 * Конвертация из DTO контакта в сущность контак
	 * 
	 * @param сontactDto
	 *            DTO контакт
	 * @return контакт
	 */
	private static Contact convertContactDto2Contact(ContactDto сontactDto) {
		Contact contact = new Contact();
		contact.setContact(сontactDto.getContact());
		contact.setType(сontactDto.getType());
		return contact;
	}

	/**
	 * Перевод строки в ContactType
	 * 
	 * @param type
	 *            строка с типом
	 * @return тип вагона
	 */
	public static ContactType createContactType(String type) {
		ContactType contactType = null;
		for (ContactType sType : ContactType.values()) {
			if (type.toUpperCase().equals(sType.getName()) || type.toUpperCase().equals(sType.name())) {
				contactType = sType;
			}
		}
		if (contactType == null) {
			contactType = ContactType.OTHER;
			LOG.warn("Unable determine type " + type);
		}
		return contactType;
	}

	/**
	 * Конвертация из сущности "станция" в dto станция
	 * 
	 * @param station
	 *            станция
	 * @return станция
	 */
	public static StationDto convertStation2StationDto(Station station) {
		StationDto stationDto = new StationDto();
		stationDto.setId(station.getId());
		stationDto.setName(station.getName());
		return stationDto;
	}

	/**
	 * Конвертация из сущности "расписания" в dto расписания
	 * 
	 * @param timetable
	 *            расписание
	 * @return расписание
	 */
	public static TimetableDto convertTimetable2TimetableDto(Timetable timetable) {
		TimetableDto timetableDto = new TimetableDto();
		timetableDto.setId(timetable.getId());
		timetableDto.setStartTime(timetable.getStartTime());
		timetableDto.setEndTime(timetable.getEndTime());
		return timetableDto;
	}

	/**
	 * Конвертация из DTO расписания" в сущности расписания
	 * 
	 * @param timetable
	 *            расписание
	 * @return расписание
	 */
	public static Timetable convertTimetable2TimetableDto(TimetableDto timetableDto) {
		Timetable timetable = new Timetable();
		timetable.setId(timetableDto.getId());
		timetable.setStartTime(timetableDto.getStartTime());
		timetable.setEndTime(timetableDto.getEndTime());
		return timetable;
	}

	public static ArchiveTicketDto convertArchiveTickets2ArchiveTicketsDto(ArchiveTicket archiveTicket) {
		ArchiveTicketDto archiveTicketDto = new ArchiveTicketDto();
		archiveTicketDto.setTrainName(archiveTicket.getTrainName());
		archiveTicketDto.setCarName(archiveTicket.getCarName());
		archiveTicketDto.setStartDate(archiveTicket.getDateTrip());
		Station stationEnd = archiveTicket.getStationEnd();
		archiveTicketDto.setStationEnd(stationEnd.getName());
		Station stationStart = archiveTicket.getStationStart();
		archiveTicketDto.setStationStart(stationStart.getName());
		archiveTicketDto.setType(archiveTicket.getCarType());
		List<ArchiveFreePlaceDto> archiveFreePlaces = new ArrayList<ArchiveFreePlaceDto>();
		archiveTicketDto.setArchiveFreePlaces(archiveFreePlaces);
		for (ArchiveFreePlace archiveFreePlace : archiveTicket.getArchiveFreePlaces()) {
			ArchiveFreePlaceDto archiveFreePlaceDto = convertArchiveFreePlace2ArchiveFreePlaceDto(archiveFreePlace);
			archiveFreePlaces.add(archiveFreePlaceDto);
		}
		return archiveTicketDto;
	}

	public static ArchiveFreePlaceDto convertArchiveFreePlace2ArchiveFreePlaceDto(ArchiveFreePlace archiveFreePlace) {
		ArchiveFreePlaceDto archiveFreePlaceDto = new ArchiveFreePlaceDto();
		archiveFreePlaceDto.setDateUpdate(archiveFreePlace.getDateUpdate());
		archiveFreePlaceDto.setFreeNumber(archiveFreePlace.getFreeNumber());
		archiveFreePlaceDto.setTariff(archiveFreePlace.getTariff());
		archiveFreePlaceDto.setTariffAlternative(archiveFreePlace.getTariffAlternative());
		archiveFreePlaceDto.setType(archiveFreePlace.getPlaceType());
		return archiveFreePlaceDto;
	}
}
