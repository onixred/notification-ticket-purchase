package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Date;
import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "поездка"
 * 
 * @author amaksimov
 */
public interface TripService {

	/**
	 * Найти список поездок в хранилище если нет, то запросить с сайта.
	 * 
	 * @param stationStartId
	 *            id точки отправления (станции/города)
	 * @param stationEndId
	 *            id точки прибытия (станции/города)
	 * @param fromDate
	 *            дата оправления
	 * @param isLoadCars
	 *            признак обновления данных о местах и вагонах
	 * @return список поездок
	 * @throws VerificationException
	 *             ошибка поиска
	 */
	List<Trip> findAllByStationAndDate(Long stationStartId, Long stationEndId, Date fromDate, boolean isLoadCars)
			throws VerificationException;

	/**
	 * Найти список поездок в хранилище если нет, то запросить с сайта.
	 * 
	 * @param nameStationStart
	 *            имя точки отправления (станции/города)
	 * @param nameStationEnd
	 *            имя точки прибытия (станции/города)
	 * @param fromDate
	 *            дата оправления
	 * @param isLoadCars
	 *            признак обновления данных о местах и вагонах
	 * @return список поездок
	 * @throws VerificationException
	 *             ошибка поиска
	 */
	List<Trip> findAllByStationAndDate(String nameStationStart, String nameStationEnd, Date fromDate,
			boolean isLoadCars) throws VerificationException;

	/**
	 * Найти все старые рейсы и добавить их в обработчик
	 */
	void findOldTripAndExecutionTasks();

	/**
	 * Найти старый рейс и добавить в обработчик
	 * 
	 * @param archiveTicketId
	 *            идентификатор рейса
	 * @throws VerificationException
	 *             ошибка поиска
	 */
	void findArchivingTripAndExecutionTasks(Long archiveTicketId) throws VerificationException;

	/**
	 * Удалить рейс по идентификатору.
	 * 
	 * @param id
	 *            идентификатор рейса
	 * @throws VerificationException
	 *             ошибка удаления
	 */
	void delete(long id) throws VerificationException;

	/**
	 * Найти рейс по идентификатору.
	 * 
	 * @param tripId
	 *            идентификатор рейса
	 * @throws VerificationException
	 *             ошибка поиска
	 */
	public Trip findById(Long tripId) throws VerificationException;

	/**
	 * Удалить рейсы по списку идентификаторов.
	 * 
	 * @param id
	 *            список идентификаторов рейсов
	 */
	void delete(List<Long> ids) throws VerificationException;

}
