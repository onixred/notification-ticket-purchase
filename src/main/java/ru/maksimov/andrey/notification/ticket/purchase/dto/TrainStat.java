package ru.maksimov.andrey.notification.ticket.purchase.dto;

import java.util.HashSet;
import java.util.Set;

/**
 * Статистика по поездам
 * 
 * @author amaksimov
 */
public class TrainStat {

	private Set<String> numbers = new HashSet<String>();
	private int freePlaces = 0;
	private double maxPrice = 0;
	private double minPrice = 0;

	public Set<String> getNumbers() {
		return numbers;
	}

	public int getFreePlaces() {
		return freePlaces;
	}

	public double getMaxPrice() {
		return maxPrice;
	}

	public double getMinPrice() {
		return minPrice;
	}

	public void setNumbers(Set<String> numbers) {
		this.numbers = numbers;
	}

	public void setFreePlaces(int freePlaces) {
		this.freePlaces = freePlaces;
	}

	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

}
