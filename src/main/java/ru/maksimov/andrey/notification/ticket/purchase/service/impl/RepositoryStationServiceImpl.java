package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.repository.StationRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryStationService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "станция"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryStationServiceImpl implements RepositoryStationService {

	@Autowired
	private StationRepository stationRepository;


	@Override
	public List<Station> findAllByNamePart(String partStationName) {
		List<Station> stations = stationRepository.findAllByNamePart(partStationName);
		return stations;
	}


	@Override
	public Station addStation(Station station) {
		Station savedStation = stationRepository.save(station);
		return savedStation;
	}

	@Override
	public Station findById(Long id) {
		return stationRepository.findOne(id);
	}

	@Override
	public Station findByСode(Long code) {
		return stationRepository.findByCode(code);
	}


	@Override
	public Station findByName(String name) {
		return stationRepository.findByName(name);
	}
}
