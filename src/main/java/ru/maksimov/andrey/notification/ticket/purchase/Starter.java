package ru.maksimov.andrey.notification.ticket.purchase;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import ru.maksimov.andrey.notification.ticket.purchase.config.SpringConfig;

/**
 * Прототип "данные о поездах"
 * 
 * @author amaksimov
 */
@SpringBootApplication()
@EnableScheduling
@ComponentScan(basePackages = {"ru.maksimov.andrey.commons", "ru.maksimov.andrey.notification" })
public class Starter {

	private static final Logger LOG = LogManager.getLogger(Starter.class);

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] { Starter.class, SpringConfig.class }, args);
		LOG.info("APPLICATION STARTED");
	}
}
