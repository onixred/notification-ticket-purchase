package ru.maksimov.andrey.notification.ticket.purchase.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveFreePlace;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.ArchiveTicket;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Place;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;

/**
 * Вспомогаткельный класс для конвертации сущностей в поток (задачу)
 * 
 * @author amaksimov
 */
public class ConvertEntity2RunnableUtil {

	/**
	 * Конвертация пути в структуру "архив билетов"
	 * 
	 * @param trip
	 *            маршрут
	 * @return список архивных билетов
	 */
	public static List<ArchiveTicket> convertTrip2ArchiveTickets(Trip trip) {
		List<ArchiveTicket> archiveTickets = new ArrayList<ArchiveTicket>();
		Date now = new Date();
		if (trip != null) {
			if (trip.getTrains().isEmpty()) {
				String noDate = "noDate";
				ArchiveTicket archiveTicket = new ArchiveTicket();
				archiveTicket.setCarType(CarType.NO_DATA);
				archiveTicket.setDateTrip(trip.getDate());
				archiveTicket.setStationEnd(trip.getStationEnd());
				archiveTicket.setStationStart(trip.getStationStart());
				archiveTicket.setCarName(noDate);
				archiveTicket.setTrainName(noDate);
				archiveTickets.add(archiveTicket);
			} else {
				for (Train train : trip.getTrains()) {
					if (train != null) {
						for (Car car : train.getCars()) {
							if (car != null) {
								ArchiveTicket archiveTicket = new ArchiveTicket();
								archiveTicket = new ArchiveTicket();
								archiveTicket.setCarType(car.getType());
								archiveTicket.setDateTrip(trip.getDate());
								archiveTicket.setStationEnd(trip.getStationEnd());
								archiveTicket.setStationStart(trip.getStationStart());
								archiveTicket.setDateUpdate(now);
								archiveTicket.setTrainName(train.getNumber());
								archiveTicket.setCarName(car.getName());
								archiveTickets.add(archiveTicket);
								for (Place place : car.getPlaces()) {
									if (place != null) {
										ArchiveFreePlace archiveFreePlace = new ArchiveFreePlace();
										archiveTicket.getArchiveFreePlaces().add(archiveFreePlace);
										archiveFreePlace.setPlaceType(place.getType());
										archiveFreePlace.setTariff(place.getTariff());
										archiveFreePlace.setTariffAlternative(place.getTariffAlternative());
										archiveFreePlace.setFreeNumber(place.getFreeNumber());
										archiveFreePlace.setDateUpdate(place.getDateUpdate());
										archiveFreePlace.setArchiveTicket(archiveTicket);
									}
								}
							}
						}
					}
				}
			}
		}
		return archiveTickets;
	}

}
