package ru.maksimov.andrey.notification.ticket.purchase.service;

import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе c отправкой сообщений
 * 
 * @author amaksimov
 */
public interface SendMessageService {

	/**
	 * Отправить сообщение
	 * 
	 * @param address
	 *            адрес получятеля
	 * @param message
	 *            сообщение
	 * @throws VerificationException 
	 */
	void sendMessage(String address, String message, String subject) throws VerificationException;

}
