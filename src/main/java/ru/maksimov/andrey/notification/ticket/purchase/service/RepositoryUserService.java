package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "пользователь"
 * 
 * @author amaksimov
 */
public interface RepositoryUserService {

	/**
	 * Добавить\обновить пользователя в хранилище
	 * 
	 * @param user
	 *            пользователь
	 * @return структура пользователь
	 */
	User addUser(User user);

	/**
	 * Найти пользователя в хранилище по логину
	 * 
	 * @param login
	 *            логин пользователя
	 * @return сущность пользовател
	 */
	User findByLogin(String login);

	/**
	 * Найти все расписания по логину.
	 * 
	 * @param login
	 *            логин пользователя
	 * @return список расписаний
	 */
	List<Timetable> findAllTimetableByLogin(String login);

	/**
	 * Найти все расписания по идентификатор пользователя.
	 * 
	 * @param id
	 *            логин пользователя
	 * @return список расписаний
	 */
	List<Timetable> findAllTimetableUserId(Long id);

	/**
	 * Добавить\обновить расписание в хранилище
	 * 
	 * @param timetable
	 *            расписание
	 * @return структура расписание
	 */
	Timetable addTimetable(Timetable timetable);

	/**
	 * Удалить расписание по идентификатору из хранилища
	 * 
	 * @param id
	 *            пользователь
	 * @return структура расписание
	 */
	void deleteTimetable(Long id);

	/**
	 * Получить количество интервалов оповещения по логину 
	 * 
	 * @param login
	 *            логин пользователя
	 * @return число интервалов
	 */
	Integer getCountTimetableByLogin(String login);
}
