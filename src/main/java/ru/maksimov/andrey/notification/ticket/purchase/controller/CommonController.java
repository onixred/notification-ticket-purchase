package ru.maksimov.andrey.notification.ticket.purchase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ru.maksimov.andrey.notification.ticket.purchase.client.NotificationTicketPurchaseClient;
import ru.maksimov.andrey.notification.ticket.purchase.client.Response;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.StationDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.UserDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.service.StationService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TripService;
import ru.maksimov.andrey.notification.ticket.purchase.service.UserService;

/* Основной контроллер 
 * 
 * @author amaksimov
 */
@RestController
@RequestMapping("rest")
public class CommonController {

	@Autowired
	private StationService stationService;

	@Autowired
	private UserService userService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private TripService tripService;

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TASK_FIND_ALL_BY_STATUS_AND_LOGIN
			+ "{login}", method = RequestMethod.POST)
	public Response<List<TaskDto>> findAllByStatusAndLogin(@RequestBody TaskStatus status,
			@PathVariable("login") String login) throws VerificationException {
		Response<List<TaskDto>> response;
		try {
			List<TaskDto> tasks = taskService.findAllByStatusAndLogin(status, login);
			response = new Response<List<TaskDto>>(tasks);
		} catch (VerificationException ve) {
			response = new Response<List<TaskDto>>(503,
					"Unable find tasks by  status and login " + login + " Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TASK_GET_COUNT_TASK_BY_STATUS_AND_LOGIN
			+ "{login}", method = RequestMethod.POST)
	public Response<Integer> getCountTaskByStatusAndLogin(@RequestBody TaskStatus status,
			@PathVariable("login") String login) throws VerificationException {
		Response<Integer> response;
		try {
			Integer count = taskService.getCountTaskByStatusAndLogin(status, login);
			response = new Response<Integer>(count);
		} catch (VerificationException ve) {
			response = new Response<Integer>(503,
					"Unable find count tasks by login " + login + " Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TASK_GET_COUNT_TASK_BY_STATUS, method = RequestMethod.POST)
	public Response<Integer> getCountTaskByStatus(@RequestBody TaskStatus status) throws VerificationException {
		Response<Integer> response;
		try {
			Integer count = taskService.getCountTaskByStatus(status);
			response = new Response<Integer>(count);
		} catch (VerificationException ve) {
			response = new Response<Integer>(503,
					"Unable find count tasks by status " + status + " Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TASK_FIND_BY_ID, method = RequestMethod.POST)
	public Response<TaskDto> findById(@RequestBody Long taskId) throws VerificationException {
		Response<TaskDto> response;
		try {
			TaskDto task = taskService.findById(taskId);
			response = new Response<TaskDto>(task);
		} catch (VerificationException ve) {
			response = new Response<TaskDto>(503,
					"Unable find task by taskId " + taskId + " Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TASK_UPDATE_BY_ID, method = RequestMethod.POST)
	public Response<Void> updateById(@RequestBody Long taskId) throws VerificationException {
		Response<Void> response;
		// TODO сразу передавать сущность а не искать в бд
		Task task = taskService.doFindById(taskId);
		if (task == null) {
			response = new Response<Void>(404, "Unable find task. taskId: " + taskId, null);
		} else {
			task.setStatus(TaskStatus.CANCEL);
			task = taskService.addTask(task);
			response = new Response<Void>(null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TASK_UPDATE, method = RequestMethod.POST)
	public Response<Void> update(@RequestBody TaskDto taskDto) throws VerificationException {
		Response<Void> response;
		try {
			taskService.addTask(taskDto);
			response = new Response<Void>(null);
		} catch (VerificationException ve) {
			response = new Response<Void>(503, "Unable update task. Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.MESSAGE_FIND_BY_TASK_ID, method = RequestMethod.POST)
	public Response<List<String>> findByTaskId(@RequestBody Long taskId) throws VerificationException {
		Response<List<String>> response;
		try {
			List<String> message = taskService.findByTaskId(taskId);
			response = new Response<List<String>>(message);
		} catch (VerificationException ve) {
			response = new Response<List<String>>(503,
					"Unable find message by TaskId:" + taskId + ". Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.STATION_FIND_ALL_BY_NAME, method = RequestMethod.POST)
	public Response<List<StationDto>> getStations(@RequestBody String name) throws VerificationException {
		Response<List<StationDto>> response;
		try {
			List<StationDto> stations = stationService.findAllByPartStationName(name);
			response = new Response<List<StationDto>>(stations);
		} catch (VerificationException ve) {
			response = new Response<List<StationDto>>(503, "Unable update task. Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.USER_ADD, method = RequestMethod.POST)
	public Response<Void> update(@RequestBody UserDto userDto) throws VerificationException {
		Response<Void> response;
		try {
			userService.addUser(userDto);
			response = new Response<Void>(null);
		} catch (VerificationException ve) {
			response = new Response<Void>(503, "Unable update user. Message: " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TIMETABLE_FIND_ALL_BY_LOGIN, method = RequestMethod.POST)
	public Response<List<TimetableDto>> findAllByLogin(@RequestBody String login) throws VerificationException {
		Response<List<TimetableDto>> response;
		try {
			List<TimetableDto> timetablesDto = userService.findAllTimetableByLogin(login);
			response = new Response<List<TimetableDto>>(timetablesDto);
		} catch (VerificationException ve) {
			response = new Response<List<TimetableDto>>(503, "Unable find all Timetables. " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TIMETABLE_GET_COUNT_TASK_BY_LOGIN, method = RequestMethod.POST)
	public Response<Integer> getCountTimetableByLogin(@RequestBody String login) throws VerificationException {
		Response<Integer> response;
		try {
			Integer countTimetable = userService.getCountTimetableByLogin(login);
			response = new Response<Integer>(countTimetable);
		} catch (VerificationException ve) {
			response = new Response<Integer>(503, "Unable get count Timetable. " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TIMETABLE_UPDATE + "{login}", method = RequestMethod.POST)
	public Response<TimetableDto> update(@RequestBody TimetableDto timetableDto, @PathVariable("login") String login)
			throws VerificationException {
		Response<TimetableDto> response;
		try {
			TimetableDto saveTimetableDto = userService.updateTimetable(timetableDto, login);
			response = new Response<TimetableDto>(saveTimetableDto);
		} catch (VerificationException ve) {
			response = new Response<TimetableDto>(503, "Unable updated Timetable. " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = NotificationTicketPurchaseClient.TIMETABLE_DELETE, method = RequestMethod.POST)
	public Response<Void> delete(@RequestBody Long id) throws VerificationException {
		Response<Void> response;
		try {
			userService.deleteTimetable(id);
			response = new Response<Void>(null);
		} catch (VerificationException ve) {
			response = new Response<Void>(503, "Unable deleted task. " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = "/findUser", method = RequestMethod.POST)
	public UserDto findUser(UserDto userDto, Model model) throws VerificationException {
		UserDto user = userService.findByLogin(userDto.getLogin());
		model.addAttribute("status", "success");
		return user;
	}

	@Loggable
	@RequestMapping(value = "/findArchivingTripAndExecutionTasks", method = RequestMethod.POST)
	public void findArchivingTripAndExecutionTasks(String strArchivingTicketId, Model model)
			throws VerificationException {
			Long archivingTicketId = Long.parseLong(strArchivingTicketId.trim());
			tripService.findArchivingTripAndExecutionTasks(archivingTicketId);
			model.addAttribute("status", "success");
			return;

	}

}
