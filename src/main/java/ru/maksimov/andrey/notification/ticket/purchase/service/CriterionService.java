package ru.maksimov.andrey.notification.ticket.purchase.service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "критерии задачи"
 * 
 * @author amaksimov
 */
public interface CriterionService {

	/**
	 * Добавить\обновить "критерии задачи"
	 * 
	 * @param criterion
	 *            критерии
	 * @return критерии
	 */
	Criterion addCriterion(Criterion criterion)
			throws VerificationException;

}
