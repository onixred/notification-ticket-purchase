package ru.maksimov.andrey.notification.ticket.purchase.service;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.UserDto;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.User;

import java.util.List;

import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "пользователь"
 * 
 * @author amaksimov
 */
public interface UserService {

	/**
	 * Найти пользователья по логину.
	 * 
	 * @param login
	 *            логин пользователя
	 * @return сущьность пользователь
	 */
	User doFindByLogin(String login) throws VerificationException;

	/**
	 * Найти пользователья по логину.
	 * 
	 * @param login
	 *            логин пользователя
	 * @param contactType
	 *            тип контакта
	 * @return сущьность пользователь
	 */
	UserDto findByLogin(String login) throws VerificationException;

	/**
	 * Добавить пользователья
	 * 
	 * @param userDto
	 *            форма пользователя
	 * @return форма пользователя
	 */
	UserDto addUser(UserDto userDto) throws VerificationException;


	/**
	 * Найти все расписания по логину.
	 * 
	 * @param login
	 *            логин пользователя
	 * @return список расписапний
	 */
	List<TimetableDto> findAllTimetableByLogin(String login) throws VerificationException;

	/**
	 * Найти все расписания по идентификатору пользователя.
	 * 
	 * @param id
	 *            идентификатор пользователя
	 * @return список расписапний
	 */
	List<TimetableDto> findAllTimetableByUserId(Long id) throws VerificationException;

	/**
	 * Найти все расписания по идентификатору пользователя.
	 * 
	 * @param id
	 *            идентификатор пользователя
	 * @return список расписапний
	 */
	List<Timetable> doFindAllTimetableByUserId(Long id) throws VerificationException;

	/**
	 * Добавить\обновить расписание в хранилище
	 * 
	 * @param tеimetableDto
	 *            расписание
	 * @param login
	 *           логин пользователя
	 * @return структура расписание
	 */
	TimetableDto updateTimetable(TimetableDto tеimetableDto, String login) throws VerificationException;

	/**
	 * Удалить расписание из хранилища
	 * 
	 * @param id
	 *            идентификатор расписания 
	 */
	void deleteTimetable(Long id) throws VerificationException;

	/**
	 * Получить количество интервалов оповещения по логину 
	 * 
	 * @param login
	 *            логин пользователя
	 * @return число интервалов
	 */
	Integer getCountTimetableByLogin(String login) throws VerificationException;
}
