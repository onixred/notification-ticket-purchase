package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.CriterionService;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryCriterionService;

/**
 * Сервис по работе с сущностью "критерии задачи"
 * 
 * @author amaksimov
 */
@Service
public class CriterionServiceImpl implements CriterionService {

	@Autowired
	private RepositoryCriterionService repositoryCriterionService;

	@Override
	public Criterion addCriterion(Criterion criterion) throws VerificationException {
		if (criterion == null) {
			throw new VerificationException("Unable add Criterion. Criterion is null");
		}
		Criterion repositoryCriterion = repositoryCriterionService.addСriterion(criterion);
		return repositoryCriterion;
	}

}
