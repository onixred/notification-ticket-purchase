package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.StationDto;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.HtmlImportService;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryStationService;
import ru.maksimov.andrey.notification.ticket.purchase.service.StationService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2DtoUtil;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertEntity2HtmlUtil;

/**
 * Сервис по работе с сущностью "станция"
 * 
 * @author amaksimov
 */
@Service
public class StationServiceImpl implements StationService {

	private static final Logger LOG = LogManager.getLogger(StationServiceImpl.class);

	@Autowired
	private HtmlImportService htmlImportService;

	@Autowired
	private RepositoryStationService repositoryStationService;

	@Override
	public List<StationDto> findAllByPartStationName(String stationName) throws VerificationException {
		List<Station> stations = doFindAllByPartStationName(stationName);
		List<StationDto> stationsDto = new ArrayList<StationDto>();
		for (Station station : stations) {
			StationDto stationDto = ConvertEntity2DtoUtil.convertStation2StationDto(station);
			stationsDto.add(stationDto);
		}
		return stationsDto;
	}

	@Override
	public List<Station> doFindAllByPartStationName(String stationName) throws VerificationException {
		if (StringUtils.isBlank(stationName)) {
			throw new VerificationException("Unable find Station. Station Name is blank");
		}
		stationName = stationName.toUpperCase();
		String partStationName = stationName;
		if (stationName.length() > 2) {
			partStationName = stationName.substring(0, 2);
		}

		List<Station> stations = repositoryStationService.findAllByNamePart(partStationName);
		// TODO учеть когда имя кривое и в бд нет смысл спрашивать на сайте ?
		filtrByStationNamePart(stations, stationName);
		if (stations.isEmpty()) {
			ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station[] htmlStations = htmlImportService
					.findAllByName(stationName);
			for (ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station htmlStation : htmlStations) {
				Station noSaveStation = ConvertEntity2HtmlUtil.convertStationHtml2Repository(htmlStation);
				try {
					stations.add(repositoryStationService.addStation(noSaveStation));
				} catch (DataAccessException dae) {
					LOG.debug("Unable save 'Station' " + noSaveStation.getName() + " Details:" + dae.getMessage());
				}

			}
		}
		return stations;
	}

	@Override
	public Station findById(Long stationId) throws VerificationException {
		return repositoryStationService.findById(stationId);
	}

	@Override
	public Station findOrSaveStation(long code, String name) throws VerificationException {
		Station station = repositoryStationService.findByСode(code);
		if (station == null) {
			station = new Station();
			station.setCode(code);
			station.setName(name);
			station = repositoryStationService.addStation(station);
		}
		return station;
	}

	@Override
	public Station findOrSaveStation(Station station) throws VerificationException {
		return findOrSaveStation(station.getCode(), station.getName());
	}

	@Override
	public Station findOrSaveStation(String name) throws VerificationException {
		// пытаемся найти БД если в БД нет то через сайт.
		List<Station> stations = doFindAllByPartStationName(name);
		if (stations.size() > 0) {
			return repositoryStationService.findByName(name);
		} else {
			throw new VerificationException("Unable find 'Station'. More than expected ");
		}
	}

	/**
	 * фильтрация станций из списка по имени
	 * 
	 * @param stations
	 *            список станций
	 * @param stationName
	 *            название станции которое нужно найти
	 */
	private void filtrByStationNamePart(List<Station> stations, String stationName) {
		stations.removeIf(station -> !station.getName().contains(stationName));
	}

}
