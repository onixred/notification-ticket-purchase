package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.Date;

import ru.maksimov.andrey.notification.ticket.purchase.entity.html.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.place.Train;
import ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip.Trip;

/**
 * Интерфейс сервиса по импортом html
 * 
 * @author amaksimov
 */
public interface HtmlImportService {

	/**
	 * Найти список станций по названию
	 * 
	 * @param stationName
	 *            название частичное или полное станции
	 * @return список станций
	 */
	Station[] findAllByName(String stationName);

	/**
	 * Найти список рейсов
	 * 
	 * @param stationStart
	 *            начальная станция
	 * @param stationName
	 *            конечная станция
	 * @param fromDate
	 *            дата отправления
	 * @return список станций
	 */
	Trip[] findAllByStationAndDate(Station stationStart, Station stationEnd, Date fromDate);

	/**
	 * Найти вагон
	 * 
	 * @param stationStart
	 *            начальная станция
	 * @param stationName
	 *            конечная станция
	 * @param fromDate
	 *            дата отправления
	 * @return список станций
	 */
	Train findByStationAndDateAndTrainNumber(Station stationStart, Station stationEnd, Date fromDate, String trainNumber);

}
