package ru.maksimov.andrey.notification.ticket.purchase.service.impl;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.config.Config;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.SendMessageService;

/**
 * Реализация сервиса по работе c отправкой электронных сообщений
 * 
 * @author amaksimov
 */
@Service
public class EmailSenderServiceImpl implements SendMessageService {

	@Autowired
	private JavaMailSender javaMailSender;

	private Config config = Config.getConfig();

	@Override
	public void sendMessage(String address, String message, String subject) throws VerificationException {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
				mimeMessage.setFrom(new InternetAddress(config.getMailNotificationSender()));
				mimeMessage.setText(message);
				mimeMessage.setSubject(subject);
			}
		};

		try {
			this.javaMailSender.send(preparator);
		} catch (MailException ex) {
			throw new VerificationException("Unable send email: ", ex);
		}
	}
}
