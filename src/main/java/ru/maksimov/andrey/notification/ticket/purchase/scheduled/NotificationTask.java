package ru.maksimov.andrey.notification.ticket.purchase.scheduled;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.client.TicketTelegramBotClient;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.MessageDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TripDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.ContactType;
import ru.maksimov.andrey.notification.ticket.purchase.config.Config;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Contact;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Timetable;
import ru.maksimov.andrey.commons.exception.SystemException;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.service.SendMessageService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TaskService;
import ru.maksimov.andrey.notification.ticket.purchase.service.TripService;
import ru.maksimov.andrey.notification.ticket.purchase.service.UserService;
import ru.maksimov.andrey.notification.ticket.purchase.utils.ConvertMessageUtil;
import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Поток оповещает пользователя. Попутно происходит обновление рейсов
 * 
 * @author amaksimov
 */
@Component
@Scope("prototype")
public class NotificationTask implements Runnable {

	private static final Logger LOG = LogManager.getLogger(NotificationTask.class);

	private static final int DELTA_SEND_MESSAGE_SEC = 60;

	private Config config = Config.getConfig();

	@Autowired
	private TripService tripService;

	@Autowired
	private SendMessageService sendMessageService;

	@Autowired
	private TicketTelegramBotClient ticketTelegramBotClient;

	@Autowired
	private UserService userService;

	@Autowired
	private TaskService taskService;

	private Long taskId;

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	@Loggable
	@Override
	public void run() {
		Task task = taskService.doFindById(taskId);
		Trip trip = task.getCriterion().getTrip();
		Long tripId = trip.getId() == null ? task.getId() : trip.getId();
		Criterion criterion = task.getCriterion();
		Set<Contact> contacts = task.getUser().getContacts();
		long stationStartId = trip.getStationStart().getId();
		long stationEndId = trip.getStationEnd().getId();
		Date fromDate = trip.getDate();

		synchronized (tripId) {
			List<TripDto> tripsDto = null;
			Date now = new Date();
			try {
				List<Trip> trips = tripService.findAllByStationAndDate(stationStartId, stationEndId, fromDate, true);
				Date dateSended = null;
				if (task.getDateSended() != null) {
					dateSended = DateUtils.addDate(task.getDateSended(), Calendar.SECOND, DELTA_SEND_MESSAGE_SEC);
				}
				tripsDto = ConvertMessageUtil.сonvertTrips2TripsDto(trips, false, dateSended, task.getDateUpdate(),
						criterion);
			} catch (VerificationException ve) {
				LOG.error("Unable task load Trip. taskId: " + taskId, ve);
			}
			Date minDateSending = DateUtils.addDate(new Date(), Calendar.MINUTE, -config.getMailNotificationInterval());
			if (tripsDto != null && !tripsDto.isEmpty() && task != null
					&& (task.getDateSended() == null || task.getDateSended().before(minDateSending))) {

				Map<String, List<String>> head2message = ConvertMessageUtil.convertTripsDto2String(tripsDto, true);

				StringBuilder skipTimetables = new StringBuilder(" ");
				if (!head2message.isEmpty()) {
					boolean isSkip = false;
					try {
						List<Timetable> timetables = userService.doFindAllTimetableByUserId(task.getUser().getId());
						LocalTime nowTime = LocalTime.now();
						if (!timetables.isEmpty()) {
							isSkip = true;
						}
						for (Timetable timetable : timetables) {
							Date startDate = timetable.getStartTime();
							Instant startInstant = Instant.ofEpochMilli(startDate.getTime());
							LocalTime startTime = LocalDateTime.ofInstant(startInstant, ZoneId.systemDefault())
									.toLocalTime();

							Date endDate = timetable.getEndTime();
							Instant endInstant = Instant.ofEpochMilli(endDate.getTime());
							LocalTime endTime = LocalDateTime.ofInstant(endInstant, ZoneId.systemDefault())
									.toLocalTime();

							skipTimetables.append("startTime ");
							skipTimetables.append(startTime);
							skipTimetables.append(" endTime ");
							skipTimetables.append(endDate);
							skipTimetables.append("\n");
							if (startTime.isBefore(nowTime) && endTime.isAfter(nowTime)) {
								isSkip = false;
								break;
							}
						}
					} catch (VerificationException ve) {
						LOG.warn("Unable find timetables: " + ve.getMessage(), ve);
					}

					if (!isSkip) {
						LOG.info("Send task taskId: " + taskId);
						for (String head : head2message.keySet()) {
							List<String> subjects = head2message.get(head);
							for (Contact contact : contacts) {
								final String subject = subjects.stream().collect(Collectors.joining("\n"));
								switch (contact.getType()) {
								case EMAIL:
									try {
										Map<String, List<String>> fullMessages = ConvertMessageUtil
												.convertTripsDto2String(tripsDto, false);
										StringBuilder messages = new StringBuilder();
										for (String fullHead : fullMessages.keySet()) {
											final String message = fullMessages.get(fullHead).stream()
													.collect(Collectors.joining("\n"));
											messages.append(message);
										}
										sendMessageService.sendMessage(contact.getContact(), messages.toString(), head);
									} catch (VerificationException ve) {
										LOG.error("Unable send message taskId: " + taskId, ve);
									}
									break;
								case Telegram:
									try {
										MessageDto messageDto = ConvertMessageUtil
												.createMessageDto(contact.getContact(), subject, head);
										ticketTelegramBotClient.notificationSend(messageDto);
									} catch (SystemException se) {
										LOG.error("Unable send message taskId: " + taskId, se);
									}
									break;
								case OTHER:
									LOG.warn("Unable send message taskId: " + taskId + " Type is "
											+ ContactType.OTHER.getName());
									break;
								}
							}
						}

						task.setDateSended(now);
					} else {
						LOG.warn("Skip send message! Closed hours! timetables: " + skipTimetables.toString());
						if (task.getDateSended() == null) {
							task.setDateSended(minDateSending);
							LOG.warn("Update send date! Closed hours! taskId:" + taskId);
						}
					}
				}
				// обновляем дату обновления
				task.setDateUpdate(now);
				taskService.addTask(task);
			} else {
				LOG.warn("Skip task taskId: " + taskId);
			}
		}
	}
}
