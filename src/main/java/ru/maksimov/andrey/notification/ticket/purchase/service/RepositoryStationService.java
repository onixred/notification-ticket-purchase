package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;

/**
 * Интерфейс сервиса по работе c хранилищем, сущность "Станция"
 * 
 * @author amaksimov
 */
public interface RepositoryStationService {

	/**
	 * Найти список станций (городов) в хранилище
	 * 
	 * @param partStationName
	 *            часть названия города
	 * @return список станций
	 */
	List<Station> findAllByNamePart(String partStationName);

	/**
	 * Добавить\обновить станцию (город) в хранилище
	 * 
	 * @param station
	 *            станция
	 * @return структура станция
	 */
	Station addStation(Station station);

	/**
	 * Найти станцию в хранилище по id
	 * 
	 * @param id
	 *            идентификатор
	 * @return станция
	 */
	Station findById(Long id);

	/**
	 * Найти станцию в хранилище по code
	 * 
	 * @param code
	 *            код (индификатор сайта)
	 * @return станция
	 */
	Station findByСode(Long code);

	/**
	 * Найти станцию в хранилище по названию
	 * 
	 * @param name
	 *            название станции
	 * @return станция
	 */
	Station findByName(String name);
	


}
