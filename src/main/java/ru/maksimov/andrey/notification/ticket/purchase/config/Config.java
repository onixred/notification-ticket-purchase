package ru.maksimov.andrey.notification.ticket.purchase.config;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.File;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Конфиг для приложения динамический
 * 
 * @author amaksimov
 */
public class Config {

	private static final Logger LOG = LogManager.getLogger(Config.class);
	private static final String FILE_NAME = "/notification-ticket-purchase.properties";

	private static final String CHARACTERS_SPLIT = "&";

	private static final String URL_POST_BASE = "html.url.post.base";
	private static final String URL_GET_CITY_FIND = "html.url.get.city.find";

	private static final String REPOSITORY_DATE_RELEVANCR_PERIOD = "repository.data.relevance.period";

	private static final String EXECUTOR_CORE_POOL_SIZE = "executor.core.pool.size";
	private static final String EXECUTOR_MAX_POOL_SIZ = "executor.max.pool.size";
	private static final String EXECUTOR_QUEUE_CAPACITY = "executor.queue.capacity";

	private static final String AUDIT_UPDATE_INTERVAL_MIN = "audit.update.interval.min";
	private static final String AUDIT_OPENING_PERIOD_TRIP = "audit.opening.period.trip";

	private static final String MAIL_NOTIFICATION_SENDER = "mail.notification.sender";
	private static final String MAIL_NOTIFICATION_INTERVAL = "mail.notification.interval.min";

	private static final String TICKET_TELEGRAM_BOT_URL_HOST = "ticket.telegram.bot.url.host";

	private static final String HTTP_CLIENT_HEADER_KEYS = "http.client.header.keys";
	private static final String HTTP_CLIENT_HEADER_VALUES = "http.client.header.values.";

	private static final String CLIENT_CONFIG_CONNECT_TIMEOUT_REGULAR = "client.config.connect.timeout.regular";
	private static final String CLIENT_CONFIG_CONNECT_TIMEOUT_MAX = "client.config.connect.timeout.max";
	private static final String CLIENT_CONFIG_CONNECTION_REQUEST_TIMEOUT_REGULAR = "client.config.connection.request.timeout.regular";
	private static final String CLIENT_CONFIG_CONNECTION_REQUEST_TIMEOUT_MAX = "client.config.connection.request.timeout.max";
	private static final String CLIENT_CONFIG_SOCKET_TIMEOUT_REGULAR = "client.config.socket.timeout.regular";
	private static final String CLIENT_CONFIG_SOCKET_TIMEOUT_MAX = "client.config.socket.timeout.max";

	private static final Config INSTANCE = new Config();
	private Configuration configuration;

	private String urlPostBase;
	private String urlGetCityFind;

	private int dateReelevancePeriodMin;

	private int corePoolSize;
	private int maxPoolSize;
	private int queueCapacity;

	private int updateIntervalMin;
	private int openingPeriodTrip;

	private String mailNotificationSender;
	private int mailNotificationInterval;

	private String ticketTelegramBotUrlHost;

	private Map<String, List<String>> headers;

	private int clientConfigConnectTimeoutRegular;
	private int clientConfigConnectTimeoutMax;

	private int clientConfigConnectionRequestTimeoutRegular;
	private int clientConfigConnectionRequestTimeoutMax;

	private int clientConfigSocketTimeoutRegular;
	private int clientConfigSocketTimeoutMax;

	private Config() {
		loadConfig();
	}

	/**
	 * @return экземпляр конфига
	 */
	public static Config getConfig() {
		return INSTANCE;
	}

	/**
	 * @return перезагрузить конфиг
	 */
	public void reload() {
		loadConfig();
	}

	public String getUrlPostBase() {
		return urlPostBase;
	}

	private void setUrlPostBase(String urlPostBase) {
		this.urlPostBase = urlPostBase;
	}

	public String getUrlGetCityFind() {
		return urlGetCityFind;
	}

	private void setUrlGetCityFind(String urlGetCityFind) {
		this.urlGetCityFind = urlGetCityFind;
	}

	/**
	 * @return период актуальности(устаревания) данных в минутах
	 */
	public int getDateReelevancePeriodMin() {
		return dateReelevancePeriodMin;
	}

	private void setDateReelevancePeriodMin(int dateReelevancePeriodMin) {
		this.dateReelevancePeriodMin = dateReelevancePeriodMin;
	}

	public int getCorePoolSize() {
		return corePoolSize;
	}

	private void setCorePoolSize(int corePoolSize) {
		this.corePoolSize = corePoolSize;
	}

	public int getMaxPoolSize() {
		return maxPoolSize;
	}

	private void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public int getQueueCapacity() {
		return queueCapacity;
	}

	private void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}

	/**
	 * Интервал обновления
	 * 
	 * @return Время открытия продажи билетов в сутках
	 */
	public int getUpdateIntervalMin() {
		return updateIntervalMin;
	}

	private void setUpdateIntervalMin(int updateIntervalMin) {
		this.updateIntervalMin = updateIntervalMin;
	}

	/**
	 * Время открытия продажи билетов
	 * 
	 * @return Время открытия продажи билетов в сутках
	 */
	public int getOpeningPeriodTrip() {
		return openingPeriodTrip;
	}

	private void setOpeningPeriodTrip(int openingPeriodTrip) {
		this.openingPeriodTrip = openingPeriodTrip;
	}

	/**
	 * @return электронная почта для отправки оповещений
	 */
	public String getMailNotificationSender() {
		return mailNotificationSender;
	}

	private void setMailNotificationSender(String mailNotificationSender) {
		this.mailNotificationSender = mailNotificationSender;
	}

	/**
	 * @return период отправления уведомлений в минутах
	 */
	public int getMailNotificationInterval() {
		return mailNotificationInterval;
	}

	private void setMailNotificationInterval(int mailNotificationInterval) {
		this.mailNotificationInterval = mailNotificationInterval;
	}

	/**
	 * Получить url-адрес TicketTelegramBot
	 * 
	 * @return url-адрес
	 */
	public String getTicketTelegramBotUrlHost() {
		return ticketTelegramBotUrlHost;
	}

	private void setTicketTelegramBotUrlHost(String ticketTelegramBotUrlHost) {
		this.ticketTelegramBotUrlHost = ticketTelegramBotUrlHost;
	}

	/**
	 * Получить "Заголовки" для http-клиента
	 * 
	 * @return "Заголовки" для http-клиента
	 */
	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	private void setHeaders(Map<String, List<String>> headers) {
		this.headers = headers;

	}

	/**
	 * Получить обычное время ожидания подключения
	 * 
	 * @return время ожидания подключения
	 */
	public int getClientConfigConnectTimeoutRegular() {
		return clientConfigConnectTimeoutRegular;
	}

	private void setClientConfigConnectTimeoutRegular(int clientConfigConnectTimeoutRegular) {
		this.clientConfigConnectTimeoutRegular = clientConfigConnectTimeoutRegular;
	}

	/**
	 * Получить максимальное время ожидания подключения
	 * 
	 * @return время ожидания подключения
	 */
	public int getClientConfigConnectTimeoutMax() {
		return clientConfigConnectTimeoutMax;
	}

	private void setClientConfigConnectTimeoutMax(int clientConfigConnectTimeoutMax) {
		this.clientConfigConnectTimeoutMax = clientConfigConnectTimeoutMax;
	}

	/**
	 * Получить обычное время ожидания запроса
	 * 
	 * @return время ожидания запроса
	 */
	public int getClientConfigConnectionRequestTimeoutRegular() {
		return clientConfigConnectionRequestTimeoutRegular;
	}

	private void setClientConfigConnectionRequestTimeoutRegular(int clientConfigConnectionRequestTimeoutRegular) {
		this.clientConfigConnectionRequestTimeoutRegular = clientConfigConnectionRequestTimeoutRegular;
	}

	/**
	 * Получить максимальное время ожидания запроса
	 * 
	 * @return время ожидания запроса
	 */
	public int getClientConfigConnectionRequestTimeoutMax() {
		return clientConfigConnectionRequestTimeoutMax;
	}

	private void setClientConfigConnectionRequestTimeoutMax(int clientConfigConnectionRequestTimeoutMax) {
		this.clientConfigConnectionRequestTimeoutMax = clientConfigConnectionRequestTimeoutMax;
	}

	/**
	 * Получить обычное время ожидания сокета
	 * 
	 * @return время ожидания сокета
	 */
	public int getClientConfigSocketTimeoutRegular() {
		return clientConfigSocketTimeoutRegular;
	}

	private void setClientConfigSocketTimeoutRegular(int clientConfigSocketTimeoutRegular) {
		this.clientConfigSocketTimeoutRegular = clientConfigSocketTimeoutRegular;
	}

	/**
	 * Получить максимальное время ожидания сокета
	 * 
	 * @return время ожидания сокета
	 */
	public int getClientConfigSocketTimeoutMax() {
		return clientConfigSocketTimeoutMax;
	}

	private void setClientConfigSocketTimeoutMax(int clientConfigSocketTimeoutMax) {
		this.clientConfigSocketTimeoutMax = clientConfigSocketTimeoutMax;
	}

	/**
	 * Загрузка конфига
	 */
	private void loadConfig() {
		String propertiesFile = null;
		String property = "notification-ticket-purchase.properties";
		try {
			propertiesFile = System.getProperty(property);
		} catch (Exception e) {
			LOG.warn("FATAL: can't load System property: " + property);
		}

		try {
			URL resource;
			if (StringUtils.isBlank(propertiesFile)) {
				resource = Config.class.getResource(FILE_NAME);
			} else {
				resource = new File(propertiesFile).toURI().toURL();
			}
			configuration = new PropertiesConfiguration(resource);
			init();
		} catch (Exception e) {
			throw new RuntimeException("FATAL: can't load config from: " + FILE_NAME, e);
		}
	}

	/**
	 * Иницилизация параметров конфига
	 */
	private void init() {
		List<Object> headerKeys = configuration.getList(HTTP_CLIENT_HEADER_KEYS);
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		for (Object headerKey : headerKeys) {
			String headerValues = configuration.getString(HTTP_CLIENT_HEADER_VALUES + headerKey);
			List<String> list = Arrays.asList( headerValues.split(CHARACTERS_SPLIT));
			headers.put((String) headerKey, list);
		}
		setHeaders(headers);
		setUrlPostBase(configuration.getString(URL_POST_BASE));
		setUrlGetCityFind(configuration.getString(URL_GET_CITY_FIND));
		setDateReelevancePeriodMin(configuration.getInt(REPOSITORY_DATE_RELEVANCR_PERIOD));
		setCorePoolSize(configuration.getInt(EXECUTOR_CORE_POOL_SIZE));
		setMaxPoolSize(configuration.getInt(EXECUTOR_MAX_POOL_SIZ));
		setQueueCapacity(configuration.getInt(EXECUTOR_QUEUE_CAPACITY));
		setUpdateIntervalMin(configuration.getInt(AUDIT_UPDATE_INTERVAL_MIN));
		setOpeningPeriodTrip(configuration.getInt(AUDIT_OPENING_PERIOD_TRIP));
		setMailNotificationSender(configuration.getString(MAIL_NOTIFICATION_SENDER));
		setMailNotificationInterval(configuration.getInt(MAIL_NOTIFICATION_INTERVAL));
		setTicketTelegramBotUrlHost(configuration.getString(TICKET_TELEGRAM_BOT_URL_HOST));

		setClientConfigConnectTimeoutRegular(configuration.getInt(CLIENT_CONFIG_CONNECT_TIMEOUT_REGULAR));
		setClientConfigConnectTimeoutMax(configuration.getInt(CLIENT_CONFIG_CONNECT_TIMEOUT_MAX));

		setClientConfigConnectionRequestTimeoutRegular(
				configuration.getInt(CLIENT_CONFIG_CONNECTION_REQUEST_TIMEOUT_REGULAR));
		setClientConfigConnectionRequestTimeoutMax(configuration.getInt(CLIENT_CONFIG_CONNECTION_REQUEST_TIMEOUT_MAX));

		setClientConfigSocketTimeoutRegular(configuration.getInt(CLIENT_CONFIG_SOCKET_TIMEOUT_REGULAR));
		setClientConfigSocketTimeoutMax(configuration.getInt(CLIENT_CONFIG_SOCKET_TIMEOUT_MAX));
	}
}
