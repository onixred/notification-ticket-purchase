package ru.maksimov.andrey.notification.ticket.purchase.service.impl;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.notification.ticket.purchase.repository.TripRepository;
import ru.maksimov.andrey.notification.ticket.purchase.service.RepositoryTripService;

/**
 * Реализация сервиса по работе c хранилищем, сущность "поездка"
 * 
 * @author amaksimov
 */
@Service
public class RepositoryTripServiceImpl implements RepositoryTripService {

	@Autowired
	private TripRepository tripRepository;

	@Override
	public Trip addTrip(Trip trip) {
		Trip savedTrip = tripRepository.save(trip);
		return savedTrip;
	}

	@Override
	public Trip findById(Long id) {
		return tripRepository.findOne(id);
	}

	@Override
	public List<Trip> findByStationAndDate(Station stationStart, Station stationEnd, Date fromDate) {
		List<Trip> trips = tripRepository.findByStation(stationStart, stationEnd, fromDate);
		return trips;
	}

	@Override
	public List<Trip> findAllByGreaterDate(Date date) {
		List<Trip> trips = tripRepository.findAllByGreaterDate(date);
		return trips;
	}

	@Override
	public List<Long> findAllIdByGreaterDate(Date date) {
		List<Long> tripIds = tripRepository.findAllIdByGreaterDate(date);
		return tripIds;
	}

	@Override
	public void delete(Trip trip) {
		tripRepository.delete(trip);
	}

	@Override
	public void delete(List<Long> ids) {
		tripRepository.delete(ids);
		
	}
}
