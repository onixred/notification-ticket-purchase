package ru.maksimov.andrey.notification.ticket.purchase.entity.html.trip;

import java.util.Date;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ru.maksimov.andrey.commons.utils.DateUtils;

/**
 * Сущность ответ на запроса получить список поездок РЖД
 * 
 * @author amaksimov
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseTrip {

	private String result;
	private Trip[] trips;
	private String transferSearchMode;
	private boolean flFPKRoundBonus;
	private Date timestamp;

	@JsonProperty("result")
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@JsonProperty("tp")
	public Trip[] getTrips() {
		return trips;
	}

	public void setTrips(Trip[] trips) {
		this.trips = trips;
	}

	@JsonProperty("TransferSearchMode")
	public String getTransferSearchMode() {
		return transferSearchMode;
	}

	public void setTransferSearchMode(String transferSearchMode) {
		this.transferSearchMode = transferSearchMode;
	}

	@JsonProperty("flFPKRoundBonus")
	public boolean isFlFPKRoundBonus() {
		return flFPKRoundBonus;
	}

	public void setFlFPKRoundBonus(boolean flFPKRoundBonus) {
		this.flFPKRoundBonus = flFPKRoundBonus;
	}

	@JsonProperty("timestamp")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DateUtils.FULL_DATE_FORMAT, timezone = "CET")
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
