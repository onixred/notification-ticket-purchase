package ru.maksimov.andrey.notification.ticket.purchase.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.CarDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.MessageDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.PlaceDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TrainDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TripDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;
import ru.maksimov.andrey.notification.ticket.purchase.dto.TrainStat;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Car;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Place;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Train;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Trip;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Criterion;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.task.Task;
import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.commons.utils.emoji.Emoji;

/**
 * Вспомогаткельный класс для конвертации в сообщение
 * 
 * @author amaksimov
 */
public class ConvertMessageUtil {

	/**
	 * Конвертация списка рейса в список соощений
	 * 
	 * @param tripsDto
	 *            список рейсов
	 * @param isShort
	 *            признак если равен истене то без подробностей о количестве
	 *            свободных мест
	 * @return список сообщений
	 */
	public static Map<String, List<String>> convertTripsDto2String(List<TripDto> tripsDto, boolean isShort) {
		Map<String, List<String>> head2message = new HashMap<String, List<String>>();
		List<String> messages = new ArrayList<String>();
		for (TripDto tripDto : tripsDto) {
			StringBuilder messageTrip = new StringBuilder();
			messageTrip.append(Emoji.AlarmClock.toString());
			messageTrip.append(" Рейс ");
			messageTrip.append(tripDto.getStationStart());
			messageTrip.append(" -> ");
			messageTrip.append(tripDto.getStationEnd());
			messageTrip.append(" отправление: ");
			String startDate = DateUtils.formatDate(tripDto.getStartDate(), DateUtils.SHORT_DATE_FORMAT_USA);
			messageTrip.append(startDate);
			messageTrip.append('\n');
			head2message.put(messageTrip.toString(), messages);
			if (!isShort) {
				for (TrainDto train : tripDto.getTrains()) {
					StringBuilder messageTrain = new StringBuilder();
					messageTrain.append(Emoji.Train.toString());
					messageTrain.append("Поезд ");
					messageTrain.append(Emoji.NumeroSign.toString());
					messageTrain.append(train.getNumber());
					messageTrain.append(" отправление: ");
					String stationStartDate = DateUtils.formatDate(train.getStationStartDate(),
							DateUtils.SIMPLE_DATE_FORMAT);
					messageTrain.append(stationStartDate);
					messageTrain.append(" прибытие: ");
					String stationEndDate = DateUtils.formatDate(train.getStationEndDate(),
							DateUtils.SIMPLE_DATE_FORMAT);
					messageTrain.append(stationEndDate);
					messageTrain.append('\n');
					for (CarDto car : train.getCars()) {
						StringBuilder messageCar = new StringBuilder();
						messageCar.append("Вагон ");
						messageCar.append(Emoji.NumeroSign.toString());
						messageCar.append(car.getName());
						messageCar.append(" Тип: ");
						messageCar.append(car.getType().getName().toLowerCase());
						messageCar.append('\n');
						for (PlaceDto place : car.getPlaces()) {
							StringBuilder messagePlace = new StringBuilder();
							messagePlace.append("Количество свободных мест: ");
							messagePlace.append(place.getFreeNumber());
							messagePlace.append('\n');
							messagePlace.append(" Тип: ");
							messagePlace.append(place.getType().getMessage());
							messagePlace.append('\n');
							messagePlace.append(" От : ");
							messagePlace.append(place.getTariff());
							messagePlace.append(Emoji.Rubl.toString());
							if (place.getTariffAlternative() != 0) {
								messagePlace.append(" до : ");
								messagePlace.append(place.getTariffAlternative());
								messagePlace.append(Emoji.Rubl.toString());
							}
							messagePlace.append('\n');
							messagePlace.append(Emoji.Date.toString());
							messagePlace.append(" обновление: ");
							String dateUpdate = DateUtils.formatDate(place.getDateUpdate(),
									DateUtils.SIMPLE_DATE_FORMAT);
							messagePlace.append(dateUpdate);
							messagePlace.append('\n');
							StringBuilder message = new StringBuilder();
							message.append(messageTrip);
							message.append(messageTrain);
							message.append(messageCar);
							message.append(messagePlace);
							messages.add(message.toString());
						}
					}
				}
			} else {

				Map<String, TrainStat> type2TrainStat = new HashMap<String, TrainStat>();
				for (TrainDto train : tripDto.getTrains()) {
					for (CarDto car : train.getCars()) {
						for (PlaceDto place : car.getPlaces()) {
							String key = "Тип вагона " + car.getType().getName().toLowerCase() + ", тип места: "
									+ place.getType().getMessage().toLowerCase();
							TrainStat trainStat = type2TrainStat.get(key);
							if (trainStat == null) {
								trainStat = new TrainStat();
								type2TrainStat.put(key, trainStat);
							}
							trainStat.setFreePlaces(trainStat.getFreePlaces() + place.getFreeNumber());
							trainStat.getNumbers().add(train.getNumber());

							double max = getMax(place.getTariff(), place.getTariffAlternative());
							double min = getMin(place.getTariff(), place.getTariffAlternative());
							if (trainStat.getMaxPrice() == 0) {
								trainStat.setMaxPrice(max);
							} else if (max > trainStat.getMaxPrice()) {
								trainStat.setMaxPrice(max);
							}
							if (trainStat.getMinPrice() == 0) {
								trainStat.setMinPrice(min);
							} else if (min < trainStat.getMinPrice()) {
								trainStat.setMaxPrice(min);
							}
						}
					}
				}
				messageTrip.append("\n");
				for (String type : type2TrainStat.keySet()) {
					TrainStat trainStat = type2TrainStat.get(type);
					Set<String> trains = trainStat.getNumbers();
					messageTrip.append(Emoji.Train.toString());
					if (trains.size() > 1) {
						messageTrip.append(" Изменения в поездах ");
					} else {
						messageTrip.append(" Изменения в поезде ");
					}
					messageTrip.append(trains.stream().map(x -> x).collect(Collectors.joining(" , ")));
					messageTrip.append(". " + type + ", количество свободных мест: " + trainStat.getFreePlaces());
					messageTrip.append(",  max цена за билет: " + trainStat.getMaxPrice());
					messageTrip.append(Emoji.Rubl.toString());
					messageTrip.append(",  min цена за билет: " + trainStat.getMinPrice());
					messageTrip.append(Emoji.Rubl.toString());
					messageTrip.append("\n");
				}
			}
			messages.add(messageTrip.toString());
		}
		return head2message;
	}

	/**
	 * Создать сообщение
	 * 
	 * @param contact
	 *            контакт
	 * @param content
	 *            содержимое сообщения
	 * @param subject
	 *            тема сообщения
	 * @return сообщение DTO
	 */
	public static MessageDto createMessageDto(String contact, String content, String subject) {
		MessageDto messageDto = new MessageDto();
		messageDto.setContent(content);
		messageDto.setSubject(subject);
		messageDto.setContactId(Long.parseLong(contact));
		return messageDto;
	}

	/**
	 * Конвертация задачи Entity в список DTO рейсов
	 * 
	 * @param task
	 *            задача с информацией о местах
	 * @param isHistoryPlace
	 *            признак нужна ли история мест
	 * @return список рейсов
	 */
	public static List<TripDto> сonvertTask2TripsDto(Task task, boolean isHistoryPlace) {
		List<TripDto> tripsDto = new ArrayList<TripDto>();
		if (task != null) {
			Criterion criterion = task.getCriterion();
			if (criterion != null) {
				Trip trip = criterion.getTrip();
				if (trip != null) {
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.YEAR, 2015);
					tripsDto = сonvertTrips2TripsDto(Collections.singletonList(trip), false, null, cal.getTime(),
							criterion);
				}
			}
		}
		return tripsDto;
	}

	/**
	 * Конвертация списка рейсов Entity в список DTO рейсов
	 * 
	 * @param task
	 *            задача с информацией о местах
	 * @param isHistoryPlace
	 *            признак нужна ли история мест
	 * @return список рейсов
	 */
	public static List<TripDto> сonvertTrips2TripsDto(List<Trip> trips, boolean isHistoryPlace, Date dateSended,
			Date dateUpdate, Criterion criterion) {
		List<TripDto> tripsDto = new ArrayList<TripDto>();
		for (Trip trip : trips) {
			TripDto tripDto = new TripDto();
			tripDto.setStationStart(trip.getStationStart().getName());
			tripDto.setStationEnd(trip.getStationEnd().getName());
			tripDto.setStartDate(trip.getDate());

			List<TrainDto> trainsDto = new ArrayList<TrainDto>();
			if (trip.getTrains() != null && !trip.getTrains().isEmpty()) {
				for (Train train : trip.getTrains()) {
					TrainDto trainDto = new TrainDto();
					trainDto.setNumber(train.getNumber());
					trainDto.setStationStartDate(train.getStationStartDate());
					trainDto.setStationEndDate(train.getStationEndDate());
					Set<CarDto> carsDto = new HashSet<CarDto>();
					for (Car car : train.getCars()) {
						if (criterion.getCarType().equals(car.getType())
								|| criterion.getCarType().equals(CarType.OTHER)) {
							CarDto carDto = new CarDto();
							carDto.setName(car.getName());
							carDto.setType(car.getType());
							for (Place place : car.getPlaces()) {
								if (criterion.getPlaceType().equals(place.getType())
										|| criterion.getPlaceType().equals(PlaceType.OTHER)) {
									PlaceDto placeDto = place.getDto();
									if (!isHistoryPlace) {
										Boolean isSave = true;
										for (PlaceDto savePlaceDto : carDto.getPlaces()) {
											if (savePlaceDto.getType().equals(placeDto.getType())) {
												isSave = false;
												// проверкак места должны быть
												// новее чем были
												if (savePlaceDto.getDateUpdate().before(placeDto.getDateUpdate())) {
													savePlaceDto.setFreeNumber(placeDto.getFreeNumber());
													savePlaceDto.setTariff(placeDto.getTariff());
													savePlaceDto.setTariffAlternative(placeDto.getTariffAlternative());
													savePlaceDto.setDateUpdate(placeDto.getDateUpdate());
												}
											}
										}
										// дата обнавления должа быть позже чем
										// дата
										// отправки. Что бы не отправлять старые
										// данные.
										if ((dateSended != null && isSave && placeDto.getDateUpdate().after(dateSended))
												|| (dateSended == null && isSave
														&& placeDto.getDateUpdate().after(dateUpdate))) {
											carDto.getPlaces().add(placeDto);
										}
									} else {
										carDto.getPlaces().add(placeDto);
									}
								}
							}
							if (!carDto.getPlaces().isEmpty()) {
								carsDto.add(carDto);
							}
						}
					}
					if (!carsDto.isEmpty()) {
						trainDto.setCars(carsDto);
						trainsDto.add(trainDto);
					}
				}
			}
			if (!trainsDto.isEmpty()) {
				tripDto.setTrains(trainsDto);
				tripsDto.add(tripDto);
			}
		}
		return tripsDto;
	}

	private static double getMin(double value1, double value2) {
		double res;
		if (value1 < value2 || value2 == 0.0d) {
			res = value1;
		} else {
			res = value2;
		}
		return res;
	}

	private static double getMax(double value1, double value2) {
		double res;
		if (value1 > value2) {
			res = value1;
		} else {
			res = value2;
		}
		return res;
	}

}
