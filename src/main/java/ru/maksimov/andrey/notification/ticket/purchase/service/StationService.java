package ru.maksimov.andrey.notification.ticket.purchase.service;

import java.util.List;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.StationDto;
import ru.maksimov.andrey.notification.ticket.purchase.entity.repository.Station;
import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Интерфейс сервиса по работе с сущностью "город"
 * 
 * @author amaksimov
 */
public interface StationService {

	/**
	 * Найти список городов (станций) в хранилище если нет то запросить с сайта.
	 * 
	 * @param stationName
	 *            часть названия города
	 * @return список городов
	 */
	List<StationDto> findAllByPartStationName(String stationName) throws VerificationException;

	/**
	 * Найти список городов (станций) в хранилище если нет то запросить с сайта.
	 * 
	 * @param partStationName
	 *            часть названия города
	 * @return список городов
	 */
	List<Station> doFindAllByPartStationName(String partStationName) throws VerificationException;

	/**
	 * Найти станцию в хранилище по id.
	 * 
	 * @param stationId
	 *            id станции
	 * @return  станция
	 */
	Station findById(Long stationId) throws VerificationException;

	/**
	 * Найти станцию в хранилище по code, если нет то сохранить как новую.
	 * 
	 * @param code
	 *            code станции
	 * @param name
	 *            название
	 * @return  станция
	 */
	Station findOrSaveStation(long code, String name) throws VerificationException;

	/**
	 * Найти станцию в хранилище по имени, если нет то сохранить как новую.
	 * 
	 * @param name
	 *            название
	 * @return  станция
	 */
	Station findOrSaveStation(String name) throws VerificationException;

	/**
	 * Найти станцию в хранилище, если нет то  сохранить как новую.
	 * 
	 * @param station
	 *            станция
	 * @return  станция
	 */
	Station findOrSaveStation(Station station) throws VerificationException;
}
